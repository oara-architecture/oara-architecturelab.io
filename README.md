# OARA Documentation

OARA Documentation

To generate the HTML documentation, you must first install Sphinx for Python3, as well as a `fulltoc` and `bibtex` extensions:
```
sudo apt install python3-sphinx
sudo apt install python3-sphinxcontrib.bibtex
sudo apt install python3-sphinxcontrib.programoutput
sudo apt install python3-sphinx-autodoc-typehints
pip3 install --user sphinxcontrib-fulltoc
pip3 install --user autodocsumm
pip3 install --user sphinx-rtd-theme
pip3 install --user sphinx-copybutton
pip3 install --user enum_tools
pip3 install --user sphinx_toolbox
```

Then just launch the generation with:
```
make html
```

To access the online version : http://oara-architecture.gitlab.io