Goals Lifecycle
===============

Goal Lifecycle Description
--------------------------

Each **Goal** has a *lifecycle*, that defines several *states*, and some *actions*
to change the goal state. This lifecycle concept has been proposed by :cite:`actorsim`.

.. figure:: goal-lifecycle.png
   :width: 300 px
   :align: center

   Goal lifecycle from :cite:`actorsim`


The goal lifecycle state have the following meaning:

* *Formulated*: the goal has just been created
* *Selected*: the goal is active, i.e. it is currently considered by the decision process
* *Expanded*: a plan to reach the goal has been computed
* *Committed*: a plan has been chosen to perform the goal
* *Dispatched*: plan execution has started
* *Evaluated*: plan execution has finished with the goal status (reached or failed)

Goal Lifecycle API
------------------

The lifecycle is implemented by the :py:class:`~oara.goal.GoalState` and
:py:class:`~oara.goal.GoalAction` enumerations.

.. autoclass:: oara.goal.GoalState
    :members:

.. autoclass:: oara.goal.GoalAction
    :members:

    .. automethod:: source

    .. automethod:: target

References
----------

.. bibliography:: refs.bib
