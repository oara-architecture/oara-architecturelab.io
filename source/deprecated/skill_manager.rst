Developping Skill Managers in ROS
=================================

OARA provides an abstract class to help the implementation of Skill Managers
in ROS, that follows the skill execution pattern.

The :py:class:`~oara.interface.SkillManager` implements a state machine, and just
asks the user to define some *hooks* in order to implement guards/actions,
and call transition on data reception.

Skill Manager State-Machine
---------------------------

The skill manager state machine is depicted in the figure below:

.. digraph:: SkillManager
    :align: center
    :graphviz_dot: circo

    INIT [style=invis];
    INIT -> UNCONFIGURED;
    UNCONFIGURED -> CONFIGURED [label=< <B>configure</B>/True/<I>configure_hook</I> >];
    CONFIGURED -> VALIDATED [label=< <B>validate</B>/validate_hook/<I>_</I> >];
    CONFIGURED -> NOT_VALIDATED [label=< <B>validate</B>/not validate_hook/<I>_</I> >];
    NOT_VALIDATED -> CONFIGURED;
    VALIDATED -> RUNNING [label=< <B>dispatch</B>/True/<I>dispatch_hook</I> >];
    RUNNING -> TERMINATED [label=< <B>terminate</B>/True/<I>_</I> >];
    RUNNING -> INTERRUPTING [label=< <B>interrupt</B>/True/<I>interrupt_hook</I> >];
    INTERRUPTING -> CONFIGURED [label=< <B>interrupted</B>/True/<I>_</I> >];
    TERMINATED -> CONFIGURED;
    VALIDATED -> INTERRUPTING;

Transitions are labeled with a triple indicating: the event that triggers the transition
(may be a function call or a message callback), the condition enabling the transition
(either True, or a call to a hook), and the action done when transiting (a call to a hook).

State-machine states are available in the API as an enumeration :py:class:`~oara.interface.SkillManagerState`.

.. autoclass:: oara.interface.SkillManagerState
    :members:
    :undoc-members:


SkillManager Interface
----------------------

In order to define your own skill manager, you must inherit the :py:class:`~oara.interface.SkillManager` class
and redefine the hooks. For skills that are not interruptible, a :py:class:`~oara.interface.UninterruptibleSkillManager` class
is also available.

.. autoclass:: oara.interface.SkillManager
    :members:

.. autoclass:: oara.interface.UninterruptibleSkillManager
    :members:
    :inherited-members:
