.. _cli_create:

Package Creation
================

OARA provices a command-line tool to create specific OARA packages defining messages types.

In this documentation, the use of the standard ROS2 package creation tool is also illustrated to
create a package for OARA modules.

Creating a package for custom interface messages
------------------------------------------------

The `create` command creates a ROS2 package that contains the necessary information to generate
OARA interface messages suited to a specific application. An example of use is given
in the `Travel tutorial <../travel/travel_root.html#creation-of-a-specific-goal-type>`__.


Basic Usage
^^^^^^^^^^^

To create the package, type:

.. code::

        ros2 oara create my_package

It will create a directory `my_package`. In this package, both goal types and
data types can be defined. 

A goal type is made of two messages:

* the base message defining the structure of the goal (e.g., a :py:class:`geometry_msgs.msg.Vector3`)
* the associated **Formulate** service used by actors to formulate goals to other actors (e.g., see :py:class:`oara_interfaces.srv.FormulateString`)

A data type is similarly defined by two messages:

* a base message defining the data structure
* an associated **Getter** service for actors to get the data from observers

For both goals and data, you can either define your own `*.msg` file in the generated package or use
a common ROS2 message.
Then, OARA provides a cmake macros to define the associated service.

Look at the generated `CMakeLists.txt` to see how to define these messages.

Some documentation on ROS2 message creation is given on the `About ROS2 Interfaces <https://index.ros.org/doc/ros2/Concepts/About-ROS-Interfaces/#interfaceconcept>`_ page.

Program options
^^^^^^^^^^^^^^^

.. program-output:: ros2 oara create --help


Creating a package to developing OARA modules
---------------------------------------------

OARA does not provide any specific command to do so. Use the standard ROS2 package creation
command.

.. note::

        As *oara* is a pure python package, it must appear in the `package.xml` only (not in a `CMakeLists.txt`).
        You must then either give both *oara* and *oara_interfaces* as dependencies when you create the package, and then
        remove *oara* from the cmake file, or give only *oara_interfaces* and add *oara* to the manifest file.


See the ROS2 documentation on `ROS2 package creation <https://index.ros.org/doc/ros2/Tutorials/Creating-Your-First-ROS2-Package/>`__.
