.. _cli-lifecycle:

Module Lifecycle Management
===========================

OARA provices a command-line tool to manage the *module lifecycle* specific OARA packages defining messages types.
The ROS2 `Node Lifecycles <https://design.ros2.org/articles/node_lifecycle.html>`__ are different from OARA Goal Lifecycles.


Configuring modules
-------------------

The ``configure`` command tries to configure *unconfigured* modules.

Program options
^^^^^^^^^^^^^^^

.. program-output:: ros2 oara configure --help


Basic Usage
^^^^^^^^^^^

To configure all OARA modules:

.. code::

        ros2 oara configure --all



Cleaning up modules
-------------------

The ``cleanup`` command tries to cleanup *configured* modules.

Program options
^^^^^^^^^^^^^^^

.. program-output:: ros2 oara cleanup --help


Basic Usage
^^^^^^^^^^^

To cleanup all OARA modules:

.. code::

                ros2 oara cleanup --all

                
Activating modules
------------------

The ``activate`` command tries to activate *inactive* modules.

Program options
^^^^^^^^^^^^^^^

.. program-output:: ros2 oara activate --help


Basic Usage
^^^^^^^^^^^

To activate all OARA modules:

.. code::

        ros2 oara activate --all


Deactivating modules
--------------------

The ``deactivate`` command tries to deactivate *active* modules.

Program options
^^^^^^^^^^^^^^^

.. program-output:: ros2 oara deactivate --help


Basic Usage
^^^^^^^^^^^

To deactivate all OARA modules:

.. code::

        ros2 oara deactivate --all


Shutting down modules
---------------------

The ``shutdown`` command tries to shutdown modules, whatever their current state.

Program options
^^^^^^^^^^^^^^^

.. program-output:: ros2 oara shutdown --help


Basic Usage
^^^^^^^^^^^

To shutdown all OARA modules:

.. code::

        ros2 oara shutdown --all
