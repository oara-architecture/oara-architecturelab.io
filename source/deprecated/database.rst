Robot Interface Database
========================

The Robot Interface Entity-Relationship Diagram is encoded as database tables.
These tables are populated with Entity instances and their relations.

The database can be managed in two frameworks:

* as an SqlLite database, stored in memory; useful for temporary use of the E-R diagram.
* as a MySQL database, stored in a server; this server would aim at storing all robot interfaces for the mission.

.. _mysql-server:

Creating an empty MySQL database
--------------------------------

First, on the server that will host the database, log into MySQL as a *root* with

.. code:: bash

  sudo mysql


and create the database. For instance, to create the *oara_robot_interface* database:

.. code:: sql

  mysql> CREATE DATABASE oara_robot_interface;


Then grant access to this database to a new user, for instance for user *oara* with password *oara*:

.. code:: sql

  mysql> GRANT ALL PRIVILEGES ON oara_robot_interface.* TO 'oara'@'localhost' BY 'oara';


Managing the database from the command line
-------------------------------------------

The **database** module provides commands to manage the MySQL database:

.. code:: bash

  $ python3 -m oara.interface.database -h

  usage: python3 -m oara.interface.database [-h] [--host HOST]
                   [--user USER] [--passwd PASSWD]
                   [--database DATABASE]
                   {create,drop,populate,show} ...

  Manage Robot Interface Database

  optional arguments:
      -h, --help            show this help message and exit
      --host HOST           MySQL host hosting the database
      --user USER           MySQL server username
      --passwd PASSWD       MySQL user password
      --database DATABASE   MySQL database

  subcommands:
    Available subcommands for interacting with the database:

    {create,drop,populate,show}
      create              create an empty database
      drop                drop all tables and data is the database
      populate            populate the database from interface descriptions
      show                show the database data


* **create**: creates the tables representing the E-R diagram
* **drop**: removes all tables and their data
* **show**: shows the content of the tables
* **populate**: populates the tables from robot YAML model files

A MySQL server must run and hold a database prior to the creation of tables. See :ref:`mysql-server` for
instructions to setup such a server.

Managing the database from Python
---------------------------------

The database can be accessed using the Singleton :py:class:`~oara.interface.RobotInterfaceDatabase`.

.. autoclass:: oara.interface.RobotInterfaceDatabase
  :members:

  .. autoattribute:: oara.interface.robot_database.MetaDB.database

  .. autoattribute:: oara.interface.robot_database.MetaDB.entity
