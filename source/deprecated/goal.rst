OARA Architecture Goals
=======================

Decision modules (aka **Actors**) in the OARA architecture exchange **Goals**.

.. toctree::
   :maxdepth: 2

   goal_lifecycle
   goal_api
