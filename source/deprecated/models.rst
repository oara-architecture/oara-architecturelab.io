Robot Interface Models
======================

Robot interfaces are either a :py:class:`~oara.interface.Data`, a :py:class:`~oara.interface.Resource`, or a :py:class:`~oara.interface.Skill`.
Their model is defined as a YAML dictionnary, whose *key* must be the element *name*.

Data
----

A :py:class:`~oara.interface.Data` is a data element provided by the robot. Its model must include the following fields:

* *type*, that must be equal to *data*
* *data_type*, that corresponds to the Python type of the data (or the ROS message type for the ROS implementation)

The model below defines a **Data** named *pose*, of type *geographic_msgs/GeoPose*:

.. code-block:: yaml
  :linenos:

  pose:
    type: data
    data_type: geographic_msgs/GeoPose


Resource
--------

A :py:class:`~oara.interface.Resource` is a shared element available on the robot. Its model must include the following fields:

* *type*, that must be equal to *resource*
* *resource_type*, that defines the type of the resource; commonly used types are 'Camera', 'Motion', ...
* *status*, that defines the default availability status of the resource; can be either *on* or *off*

The model below defines a *Motion* **Resource** named *motion*, whose default *status* is *on*:

.. code-block:: yaml
  :linenos:

  motion:
    type: resource
    resource_type: Motion
    status: on

Skill
-----

A :py:class:`~oara.interface.Skill` is a function or service provided by the robot that can be triggered by the decision and control architecture.
A :py:class:`~oara.interface.Skill` has the following fields:

* *type*: must be equal to *skill*
* *inputs*: describes the inputs parameters of the skill as a dictionary of (variables, types)
* *resources*: list the resources related to the skill and the usage modality; this modality may be one of:

  * *uses*: the skill will use the resource during its execution
  * *produces*: the skill execution will activate the production of the resource status

* *data*: list the data related to the skill and the usage modality; this modality may be one of:

    * *reads*: the skill will read the data
    * *produces*: the skill execution will activate the production of the data

* *precondition*: defines a precondition to be checked before triggering the execution

* *interruptible*: defines whether the skill can be interrupted once started; can be *true* or *false*
* *failures*: defines the possible failure modes of the skill, as a list of mode names
* *progress*: defines the kind of data given by the skill during its execution; it is a dictionary containing the following fields, set to either *on* or *off* (default):

  * *time*: the skill will give an estimation of the remaining time to success
  * *realization*: the skill will give an estimation of the realization progress, as a ration between 0 and 1
  * *success_probability*: the skill will give an estimation of chance of success, as a ratio between 0 and 1
  * *period*: the period at which the progress element are sent by the skill (value in seconds)

* *stop_condition*: in case the skill has no intrinsic objective, a stop condition is given by the control architecture; may be one of:

  * *interrupt*: the skill is stopped when an interruption is triggered


The following code defines a *goto* skill:

.. code-block:: yaml
  :linenos:

  goto:
    type: skill
    inputs:
      target: geographic_msgs/GeoPoint
      speed: float64
    resources:
      motion: uses
    data:
      pose: reads
    interruptible: true
    failures: [BLOCKED]
    progress:
      time: on
      realization: on
      success_probability: off
      period: 1
