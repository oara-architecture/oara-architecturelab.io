Goals API
=========

Goals are just objects that provide some serialization/deserialization in a YAML
format in order to be exchanged using a standard protocol.

Goals
-----

All goals must then inherit the :py:class:`oara.goal.Goal` abstract class.

.. autoclass:: oara.goal.Goal
    :members:


To define a new goal class, you just have to define a new subclass of :py:class:`oara.goal.Goal`,
and redefine the constructor by using named arguments to define the goal attributes.
For instance, a **PatrolGoal** defined by a *path*, a *sensor*, and a *start_time*,
can be defined as:

.. code-block:: python

  class PatrolGoal(oara.goal.Goal):
    def __init__(self, path=[], sensor='', start_time=math.inf):
      oara.goal.Goal.__init__(self, path=path, sensor=sensor, start_time=start_time)

Goals ID
--------

Goals are identified using
`Universally Unique Identifiers <https://en.wikipedia.org/wiki/Universally_unique_identifier>`_.
The :py:class:`~oara.goal.GoalID` class provides a wrapper of :py:class:`uuid.UUID` to make IDs hashable.

.. autoclass:: oara.goal.GoalID
    :members:
