Entity-Relationship Diagram
===========================

Robot interface to OARA are made of three kind of elements:

* :py:class:`~oara.interface.Data`, that represent the data provided by the robot,
* :py:class:`~oara.interface.Resource`, that represent some resources available on the robot,
* :py:class:`~oara.interface.Skill`, that represent the capabilities of the robot, in term of action or service it can perform.

These elements are described by the following Entity-Relationship Diagram:

.. figure:: oara_robot_interface.png
   :width: 100%
   :align: center

   Entity-Relationship Diagram of the Robot Interface

Each entity is described below:

Robot Entity
------------

.. autoclass:: oara.interface.Robot
  :members:


ResourceType Entity
-------------------

.. autoclass:: oara.interface.ResourceType
  :members:


Resource Entity
---------------

.. autoclass:: oara.interface.Resource
  :members:


Data Entity
-----------

.. autoclass:: oara.interface.Data
  :members:


Skill Entity
------------

.. autoclass:: oara.interface.Skill
  :members:
