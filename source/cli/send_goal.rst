.. _cli_send_goal:

Sending Goals
=============

OARA provides a command-line tool to send a goal to an actor.
The type of goal and its value are passed as command-line arguments.

This command will first create a :ref:`uuid` for the goal, and then formulate
the goal to the actor. While the several steps succeed, the goal will evolve
along its lifecycle.

If any step fails, the goal will be dropped from the actor.

As standard ROS2 commands, the ``send_goal`` OARA command provides auto-completion for ROS2 types and message prototypes.

Program options
^^^^^^^^^^^^^^^

.. program-output:: ros2 oara send_goal --help


Usage Examples
--------------

To send a goal to a ``/print_controller`` actor accepting *String* goals:

.. code:: 

        ros2 oara send_goal /print_controller std_msgs/String "data: 'hello'"

