.. _print_controller:

Print Controller
================

OARA provides a command-line tool to run a **Controller** that prints goals in the terminal. The type of accepted goal can be defined when running the command.

As standard ROS2 commands, the ``print`` OARA command provides auto-completion for ROS2 types.

Most of the common ROS2 messages have been wrapped into goal specification messages in the `oara_std_interfaces`, `oara_sensor_interfaces`, `oara_geometry_interfaces` and `oara_geographic_interfaces` packages.

Program options
---------------

.. program-output:: ros2 oara print --help


Usage Examples
--------------

To create a controller accepting `std_msgs/msg/String <https://github.com/ros2/common_interfaces/blob/master/std_msgs/msg/String.msg>`__ as goals:

.. code:: 

        ros2 oara print oara_std_interfaces/msg/StringGoal


To create a controller accepting `std_msgs/msg/Int32 <https://github.com/ros2/common_interfaces/blob/master/std_msgs/msg/Int32.msg>`__ as goals, and waiting 3 seconds before finishing the goal:

.. code:: 

        ros2 oara print oara_std_interfaces/msg/Int32Goal --sleep 3