Command-Line Interface
======================

OARA provides a ROS2 compliant command-line interface including several commands to generate packages
or interact with modules at execution.


.. toctree::
   :maxdepth: 2

   list
   send_goal
   get_data
   print_controller


..create
..lifecycle
