.. _cli_get_data:

Getting Data
============

OARA provides a command-line tool to get data owned by an observer.
The observer node and the data name are passed as command-line arguments.

Program options
^^^^^^^^^^^^^^^

.. program-output:: ros2 oara get_data --help


Usage Examples
--------------

To get a `loc` data owned by a ``/travel_observer`` node:

.. code:: 

        ros2 oara get_data /travel_observer loc

