.. _cli_list:

List OARA Modules
=================

OARA provides a command-line tool to list the running modules,
and print their current node state.

Program options
^^^^^^^^^^^^^^^

.. program-output:: ros2 oara list --help

Display
^^^^^^^

This command displays a table of the detected OARA module nodes, along with the type of module (actor, observer), 
its current state, and a timer that displays the time elapsed since the last message received from this module.

.. code-block:: bash
    
              ___           ___           ___           ___     
             /\  \         /\  \         /\  \         /\  \    
            /::\  \       /::\  \       /::\  \       /::\  \   
           /:/\:\  \     /:/\:\  \     /:/\:\  \     /:/\:\  \  
          /:/  \:\  \   /::\~\:\  \   /::\~\:\  \   /::\~\:\  \ 
         /:/__/ \:\__\ /:/\:\ \:\__\ /:/\:\ \:\__\ /:/\:\ \:\__\
         \:\  \ /:/  / \/__\:\/:/  / \/_|::\/:/  / \/__\:\/:/  /
          \:\  /:/  /       \::/  /     |:|::/  /       \::/  / 
           \:\/:/  /        /:/  /      |:|\/__/        /:/  /  
            \::/  /        /:/  /       |:|  |         /:/  /   
             \/__/         \/__/         \|__|         \/__/    

        ┏━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━┳━━━━━━━━━━━━━━┳━━━━━━━━━━━━┳━━━━━━━━━━━━┓
        ┃ Node               ┃ Module            ┃ Type     ┃        State ┃      Delay ┃  Last seen ┃
        ┡━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━╇━━━━━━━━━━━━━━╇━━━━━━━━━━━━╇━━━━━━━━━━━━┩
        │ /walk_actor        │ walk_actor        │ ACTOR    │       ACTIVE │     0.0012 │     2.4478 │
        │ /travel_observer   │ travel_observer   │ OBSERVER │       ACTIVE │     0.0009 │     2.4467 │
        │ /pay_taxi_actor    │ pay_taxi_actor    │ ACTOR    │       ACTIVE │     0.0011 │     2.4453 │
        │ /call_taxi_actor   │ print_controller  │ ACTOR    │       ACTIVE │     0.0012 │     2.3022 │
        │ /travel_taxi_actor │ travel_taxi_actor │ ACTOR    │       ACTIVE │     0.0062 │     2.4367 │
        │ /ride_taxi_actor   │ ride_taxi_actor   │ ACTOR    │       ACTIVE │     0.0023 │     2.4356 │
        │ /travel_htn_actor  │ travel_htn_actor  │ ACTOR    │       ACTIVE │     0.0012 │     2.3089 │
        └────────────────────┴───────────────────┴──────────┴──────────────┴────────────┴────────────┘
