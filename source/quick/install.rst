Install Instructions
====================

To install OARA, you must first install ROS2 (Humble recommended) on your system: see `ROS2 Humble Install <https://docs.ros.org/en/humble/Installation.html>`__ instructions.


Then create your workspace and install OARA. OARA is made of several packages:

* `oara_interfaces` that contains some standard ROS2 messages for OARA
* `oara` in itself, as a Python library to define modules and decision architectures
* `oara_std_interfaces` that contains OARA wrappers for `std_msgs` messages
* `oara_sensor_interfaces` that contains OARA wrappers for `sensor_msgs` messages
* `oara_geometry_interfaces` that contains OARA wrappers for `geometry_msgs` messages
* `oara_geographic_interfaces` that contains OARA wrappers for `geographic_msgs` messages
* `ros2oara`, a ROS2 command-line tool for OARA

Dependencies
------------

In addition to the basic ROS2 install, the following tools or packages are needed:

* ROS2 Lifecycle Manager:

.. code::

        sudo apt install ros-humble-nav2-lifecycle-manager


* Package Installer for Python3:

In case you do not have ``pip3`` yet, install it:

.. code::

        sudo apt install python3-pip 


* Command-Line Interface libraries:

Some OARA tools use nice command-line interface packages:

.. code::

        pip3 install rich pyfiglet pyrsistent --user


* Reelay Monitors:

`Reelay <https://doganulus.github.io/reelay/>`__ is a library that implements Runtime Verification
monitors based on Past-Time Metric Temporal Logic :cite:`DBLP:journals/corr/abs-1901-00175`. This logic and the Reelay monitors are used
in the specification and management of OARA *events*.

.. code::

        pip3 install reelay --user

        
* Networkx:

`Netwokx <https://networkx.org/>`__ is a library that implements Networks and Graph structures and algorithms.
It is used to represent *Plan* data structures.
        
.. code::
        
        pip3 install networkx --user

        
* Graphviz

Graphviz is used to manage the graph of OARA architectures in some tools. To install the `PyGraphviz <https://pygraphviz.github.io/>`__ package:

.. code::

        sudo apt-get install graphviz graphviz-dev
        pip3 install pygraphviz --user


ROS2 OARA workspace
-------------------

For the moment, the ROS2 `Managed Nodes <http://design.ros2.org/articles/node_lifecycle.html>`__ are
only implemented in C/C++. While *rclpy_lifecycle* is not part of the official ROS2 distribution,
you must download a custom implementation of `rclpy_lifecycle <https://github.com/lesire/rclpy_lifecycle>`__.

The basic installation requires the packages listed in the following commands. 
Other packages (such as wrappers for sensor or geometry messages) can be installed depending on the needs for your architecture.

.. code::

        mkdir -p ~/oara_ws/src
        cd ~/oara_ws/src
        git clone https://github.com/lesire/rclpy_lifecycle.git
        git clone https://gitlab.com/oara-architecture/oara_interfaces
        git clone https://gitlab.com/oara-architecture/oara
        git clone https://gitlab.com/oara-architecture/ros2oara
        git clone https://gitlab.com/oara-architecture/oara_std_interfaces
        cd ..
        source /opt/ros/humble/setup.bash
        colcon build
