.. _quick-travel:

The Travel Architecture
=======================

In this section, we will run the OARA architecture that implements the **Travel HTN**
resolution. The *travel* problem is a famous and standard problem in Hierarchical Task Network (`HTN <https://en.wikipedia.org/wiki/Hierarchical_task_network#:~:text=In%20artificial%20intelligence%2C%20hierarchical%20task,form%20of%20hierarchically%20structured%20networks.>`__)
Planning.

The task to achieve is to *travel* to a desired location. Two possible methods can be used:

* *travel-by-taxi*: it consists in *calling* a taxi, *riding* to the destination, and *paying* the taxi fare, if we have enough money!
* *travel-by-foot*: it consists in *walking* to the destination, but will take some time... and may even not be possible if the destination is too far away!

The standard HTN problem consists is solving the *travel* task, given an initial location, a
desired destination, and an amount of money.
The left figure depicts the HTN model of the Travel planning problem.


.. list-table::
    :header-rows: 1
    :widths: 40 40
    :align: center

    * - .. graphviz:: /_static/images/tdg-lifted.dot
                :align: center
      - .. image:: /_static/images/travel-architecture.png
                :align: center
                :width: 600
    * - Travel HTN
      - Travel OARA Architecture


On the right, the OARA architecture that implements the planning and execution of the same problem.

In the following, you will download and execute this deliberative architecture based on OARA 
to solve and execute the *travel* task, while managing several reasoning levels, 
and possible disturbances (*"Hey, a 100€ bill on the sidewalk!!"*).

The step-by-step development of this architecture is described in :ref:`travel-tutorial`.

Install the *travel_oara_tutorial* packages
-------------------------------------------

The (working!) code of the Travel tutorial is available on the repository `<https://gitlab.com/oara-architecture/travel_oara_tutorial>`__.
To install it:

.. code:: bash

        cd ~/oara_ws/src
        git clone https://gitlab.com/oara-architecture/travel_oara_tutorial
        cd ~/oara_ws
        source install/setup.bash
        colcon build


.. warning::

        Take care that ROS (colcon actually) does not accept several packages with the same name.
        If you want to install the *ready-to-work* tutorial and also follow the step-by-step tutorial, 
        you will have to create (``touch``) an empty ``COLCON_IGNORE`` file in the directory containing 
        the tutorial version you would like colcon to ignore.


The top actor that manages the mission planning uses Hierarchical Task Networks. You need to install
the Python version of the SHOP planner (a famous HTN solver) to solve such problems:

.. code::

        cd
        git clone https://github.com/lesire/pyhop.git
        cd pyhop
        python3 setup.py install --user


Launch the OARA architecture
----------------------------

Two OARA architectures are available in the tutorial. The first one performs
hierarchical planning and goal decomposition. The second one includes failure and event management 
(and associated replanning).

To launch the basic one:

.. code:: bash

        source ~/oara_ws/install/setup.bash
        ros2 launch travel_tutorial travel_architecture_simple.launch.py


To launch the architecture with replanning:

.. code:: bash

        source ~/oara_ws/install/setup.bash
        ros2 launch travel_tutorial travel_architecture.launch.py



Visualize the architecture
--------------------------

OARA comes with a few tools to help you visualize and interact with an OARA architecture. You can also use standard ROS tools.

To visualize the list of OARA modules and their current state, use the following command:

.. code:: bash

        source ~/oara_ws/install/setup.bash
        ros2 oara list


To display the ROS graph of nodes and topics, use the following command:

.. code:: bash

        ros2 run rqt_graph rqt_graph


*Travel!*
---------


.. code:: bash

        source ~/oara_ws/install/setup.bash
        ros2 oara send_goal /travel_htn_actor travel_tutorial_interfaces/msg/TravelDestination 'destination: 15'


If you give a destination very close to the current location (lower than 2 cells), you should *walk*.
Otherise, you should *ride a taxi* only if you have enough cash!