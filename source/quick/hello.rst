The Hello World Actor
=====================

In this small example, we will run a simple **Actor** from the command line, that passes
successfully all the steps of the goal lifecycle, and prints the goal message.

Goal Lifecycle
--------------

Each **Goal**, managed within an actor, has a **Lifecycle**, that defines the current goal state
along with the evolution of this state.

The lifecyle managed by an actor is shown in the figure below:

.. image:: /_static/images/lifecycle-actor.png
  :width: 400
  :align: center
  :alt: Goal Lifecycle


The goal states have the following meaning:

* *FORMULATED*: the goal is known by the actor, stored in its memory;
* *SELECTED*: the goal is selected, some actors can manage several selected goals at the same time, some others cannot;
* *EXPANDED*: a decomposition (or plan) has been computed to execute this goal;
* *COMMITTED*: a plan has been selected for this goal;
* *DISPATCHED*: the goal is being executed;
* *EVALUATED*: the goal execution is being evaluated; it may be triggered by an incoming external event or an execution report;
* *FINISHED*: when the goal execution is finished, it ends and is removed from the actor memory;
* *DROPPED*: when the actor is asked to drop a goal, it removes the goal from its memory, whatever its current state

Actor Interface
---------------

Each **Actor** provides an interface that allows to request the several steps of the managed goals lifecycle. 
These steps, that can be triggered by *clients*, are displayed with plain arrows in the figure above.

When a goal is first formulated, the goal is identified by a unique identifier (:py:class:`oara.UID`), that comes with the goal data structure.
Then, all subsequent steps (selection, expansion, ...) only use the goal identifier.

After each step request, the actor will advertise the new state of the goal, that may have evolved if the request has been accepted, or that may still be the same if the request has been rejected.
All transitions, including the ones that do not correspond to requests (displayed with dashed arrows) also lead to a publication of the new goal state.

Deploying the Hello World actor
-------------------------------

With the base OARA installation comes a set of useful command-line tools to interact with the OARA ecosystem. One of these tools starts an actor following the *Controller*
pattern that accepts goals whose type is defined in the command-line, and that prints the goal value on dispatch.

A **Controller** is a specific actor that does not expand the goal into subgoals. Instead, it directly executes the goal, possibly converting it into another format.
Controllers are subclasses of actors, and they must only define how they *select* goals, i.e. if they accept to activate a goal lifecycle, and how they *dispatch* goals, i.e. how they execute them.

The controller we consider here always accepts goal selection (its select method always return ``True``), and the dispatch step just prints the goal value, and waits for a given delay before finishing the goal.

This controller is the :ref:`print_controller`. Here we will start the :ref:`print_controller` with *String* goals and a delay of one second:

.. code-block:: bash

        ros2 oara print oara_std_interfaces/msg/StringGoal --sleep 1.0

It should display the following output in the terminal:

.. code-block:: bash

              ___           ___           ___           ___     
             /\  \         /\  \         /\  \         /\  \    
            /::\  \       /::\  \       /::\  \       /::\  \   
           /:/\:\  \     /:/\:\  \     /:/\:\  \     /:/\:\  \  
          /:/  \:\  \   /::\~\:\  \   /::\~\:\  \   /::\~\:\  \ 
         /:/__/ \:\__\ /:/\:\ \:\__\ /:/\:\ \:\__\ /:/\:\ \:\__\
         \:\  \ /:/  / \/__\:\/:/  / \/_|::\/:/  / \/__\:\/:/  /
          \:\  /:/  /       \::/  /     |:|::/  /       \::/  / 
           \:\/:/  /        /:/  /      |:|\/__/        /:/  /  
            \::/  /        /:/  /       |:|  |         /:/  /   
             \/__/         \/__/         \|__|         \/__/    

        [INFO 1644488199.489039865] [print_controller]: PrintController for task StringGoal


Activating an actor
-------------------

OARA Modules implement the ROS2 `Lifecycle <https://design.ros2.org/articles/node_lifecycle.html>`__ state-machine for nodes. When created, OARA modules are initially *Unconfigured*. 
They must then be **configured**, and further **activated** before being fully operational.

.. warning::

        ROS2 Lifecycles correspond to *Node* execution behavior. The lifecycle of nodes in ROS2 in completely disconnected from the *Goal Lifecycle* in OARA.
        Except that an actor will not accept any incoming request if it is not **activated**!


When launched, our :ref:`print_controller` is then *unconfigured*. To check the current status of all existing OARA modules, you can use the :ref:`cli_list` tool:

.. code::

        ros2 oara list


You should see the ``/print_controller`` as the only existing actor, and that it is *unconfigured*:

.. code::

        ┏━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━┳━━━━━━━┳━━━━━━━━━━━━━━┳━━━━━━━━━━━━┳━━━━━━━━━━━━┓
        ┃ Node              ┃ Module           ┃ Type  ┃        State ┃      Delay ┃  Last seen ┃
        ┡━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━━╇━━━━━━━╇━━━━━━━━━━━━━━╇━━━━━━━━━━━━╇━━━━━━━━━━━━┩
        │ /print_controller │ print_controller │ ACTOR │ UNCONFIGURED │     0.0014 │     0.7905 │
        └───────────────────┴──────────────────┴───────┴──────────────┴────────────┴────────────┘



The management of the modules lifecycles is possible using the standard ROS2
`Lifecycle Management tools <https://github.com/ros2/demos/blob/master/lifecycle/README.rst>`__.

To configure the ``/print_controller`` actor:

.. code::

        ros2 lifecycle set /print_controller configure


If the configure succeeded, the ``/print_controller`` actor can be activated using:

.. code::

        ros2 lifecycle set /print_controller activate


Sending an Hello World goal
---------------------------

OARA provides a command-line tool to send a goal to an actor and make this goal evolve along its lifecycle. 
This command is the :ref:`cli_send_goal` command.

Send an "HELLO" goal to the ``/print_controller`` actor and look at both terminal
to see the goal evolution!

.. code::

        ros2 oara send_goal /print_controller std_msgs/msg/String "data: 'HELLO'"

