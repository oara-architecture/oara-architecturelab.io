.. _travel-tutorial:

The *Travel* OARA Architecture Tutorial
=======================================

In this section, we will develop the OARA architecture to implement the **Travel HTN**
mission. The *travel* problem is a famous and standard problem in Hierarchical Task Network (`HTN <https://en.wikipedia.org/wiki/Hierarchical_task_network#:~:text=In%20artificial%20intelligence%2C%20hierarchical%20task,form%20of%20hierarchically%20structured%20networks.>`__) Planning. The HTN of the simplest travel problem is depicted below:

.. graphviz:: /_static/images/tdg-lifted.dot
        :align: center
        :caption: Travel HTN

The task to achieve is to *travel* to a desired location. Two possible methods can be used:

* *travel-by-taxi*: it consists in *calling* a taxi, *riding* to the destination, and *paying* the taxi fare, if we have enough money!
* *travel-by-foot*: it consists in *walking* to the destination, but will take some time... and may even not be possible if the destination is too far away!

The standard HTN problem consists is solving the *travel* task, given an initial location, a
desired destination, and an amount of money.

In the following, you will develop a deliberative architecture based on OARA 
to solve and execute the *travel* task, while managing several reasoning levels, 
and possible disturbances (*"Hey, a 100€ bill on the sidewalk!!"*). You will implement the OARA architecture
depicted in the following figure:

.. image:: /_static/images/travel-architecture.png
        :align: center
        :width: 800


To see the resulting architecture in practice, go to the :ref:`Quick Start Travel Tutorial <quick-travel>`.


.. toctree::
   :maxdepth: 1

   create_pkg
   call
   ride
   observer
   pay
   walk
   travel_taxi
   travel_root
   replanning
   events