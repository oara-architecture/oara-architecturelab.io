The *ride* actor: a simple publisher controller
===============================================

In this tutorial, we will create our first **Actor**, that
will manage to *ride the taxi* to our final destination. This actor is a **Controller**: it does not perform any goal expansion, it just dispatches the goal it receives. :ref:`Controllers <oara-controller>` implement a partial Goal lifecycle.

.. image:: /_static/images/lifecycle-controller.png
  :width: 500
  :align: center
  :alt: Controller Goal Lifecycle


The **ride** actor will accept goals corresponding to the destination to reach,
represented by an integer value. Then, they will *simulate* a control of an
underlying functional architecture by publishing the destination on a ROS2 topic.

Destination goal messages
-------------------------

The goals accepted by our actor, that correspond to destinations we want to reach, are represented as integers, using the standard *std_msgs/msg/Int32* ROS message type.
To be manipulated by OARA actors, these goal types must be prefixed with a header that allows actors to follow the current state of each goal.
This header is provided as the *oara_interfaces/msg/GoalRequest* message. To have a look at its containt, type the following command in a terminal:


.. command-output:: ros2 interface show oara_interfaces/msg/GoalRequest


This message contains a standard ROS header, unique identifiers to manage the goal, and the current goal state (as defined in the lifecycle presented above).

OARA comes with common goal messages that already aggregate this header with the ROS common messages. For instance, if you have correctly built the *oara_std_interfaces* package, 
you should see the containt of the *oara_std_interaces/msg/Int32Goal* message:

.. command-output:: ros2 interface show oara_std_interfaces/msg/Int32Goal


We will use the goal messages provided in the *oara_std_interaces* package in the Travel tutorial.


The *ride_taxi_actor* script
----------------------------

In the *travel_tutorial* package, create a ``src`` folder, where we will put all the local scripts.
Add a ``src/ride_taxi_actor.py`` file, and fill it with the following code:

.. code-block:: Python
        :linenos:

        #!/usr/bin/env python3
        import oara
        import oara.actors

        from std_msgs.msg import Int32
        from oara_std_interfaces.msg import Int32Goal


        class RideTaxiActor(oara.actors.Controller):

            def __init__(self, name: str):
                super().__init__(name, Int32Goal)

            def on_activate(self, state):
                self.get_logger().info("Creating 'loc' publisher")
                self.loc_pub = self.create_publisher(Int32, "loc", 1)
                return super().on_activate(state)

            def on_deactivate(self, state):
                self.destroy_publisher(self.loc_pub)
                return super().on_deactivate(state)

            def select(self, goal_id, goal):
                return True

            def dispatch(self, goal_id, goal, plan):
                self.get_logger().info(f"Arrived at location {goal.data}")
                self.loc_pub.publish(goal)
                return True

            def monitor(self, goal_id):
                self.finish(goal_id)

            def drop(self, goal_id):
                return True


        if __name__ == '__main__':
            oara.main(RideTaxiActor, "ride_taxi_actor")


Let's analyse this code. The shebang on line 1 is needed to directly run the script without calling python3 first. 
Then we import the *oara* library, and its *actors* package.

Line 5 imports the ROS2 package containing the message we will publish to control the taxi on the functional layer.

Line 6 imports the message corresponding to the kind of goals accepted by our actor: *Int32* goals wrapped in the *oara_std_interaces* packages.


From line 9, we then create the class of our *RideTaxiActor*:

* it inherits from an OARA :py:class:`oara.actors.Controller`;
* its constructor calls the :py:meth:`oara.actors.Controller`'s constructor, with the actor name, and the type of the accepted goals;
* on activation, it creates a ROS2 publisher to send the destination to the *loc* ROS2 topic.
* on deactivation, it destroys this publisher, so that successive activation/deactivation will not create a large number of publishers!

Then, some Controller methods *must* be specialized; they correspond to the steps
of the Goal Lifecyle needed for a Controller actor, displayed in purple in the Controller lifecycle figure:

* selection always returns ``True``: all goal selection requests are accepted by our actor;
* dispatching prints a message, then publishes the goal on the ROS2 topic;
* monitoring directly sets the goal as *finished*, as once the message is published to *loc*, no acknowledgement from the functional layer is expected;
* when dropping the goal, no interaction with the functional layer is necessary.

Finally, on lines 38 and 39, we define the entry point, and use the 
:py:func:`oara.main` function to initialize and create our actor; this function takes the OARA module class as first argument, and then the list of parameters passed to its constructor (here only the name of the actor, see line 11).


Add the *ride_taxi_actor* to the architecture deployment
--------------------------------------------------------

Edit the ``travel_architecture.launch.py`` file, created in the previous tutorial,
and add this new actor. You must insert the following code in the ``generate_launch_description`` function, and then add the created object in the returned ``LaunchDescription``:

.. code-block:: python
        :linenos:

        ride_taxi_actor = LifecycleNode(
            package='travel_tutorial',
            executable='ride_taxi_actor.py',
            name='ride_taxi_actor',
            namespace="",
            output="both")


.. code-block:: python
        :linenos:

        return LaunchDescription([call_actor, ride_taxi_actor])


.. warning::

    For a Python script that contains a ROS2 Node to be executed by ROS2 (typically using ``ros2 run`` or ``ros2 launch``),
    it needs to be *executable*! Use the following command to change the rights of your script:

    .. code-block::

        chmod a+x ~/oara_ws/src/travel_tutorial/src/ride_taxi_actor.py


Automatic module activation
---------------------------

We will see here how to automatize the configuration/activation of the several modules from the launch file. 
If you need to follow a strict configuration and activation process (for instance, a specific order between your modules), you can either write a script outside the launch file 
that will call the `ros2 lifecycle set` commands, or use launch *events* related to lifecycle nodes (unfortunately, there is no ROS2 tutorial yet, if you are interested with these concepts, you must navigate in the `launch_ros` repository).

Here, we will use a tool developed as part of the Nav2 framework to configure and activate a set of nodes, the `Lifecycle Manager <https://navigation.ros.org/configuration/packages/configuring-lifecycle.html?highlight=lifecycle>`__.

First, install the corresponding package:

.. code-block:: bash

    sudo apt install ros-humble-nav2-lifecycle-manager


Then, add the node to the launch file:

.. code-block:: python
        :linenos:

        lifecycle_manager = Node(
            package="nav2_lifecycle_manager",
            executable="lifecycle_manager",
            namespace="",
            name="lifecycle_manager",
            parameters=[{
                "autostart": True,
                "bond_timeout": 0.0,
                "node_names": [
                    "call_taxi_actor",
                    "ride_taxi_actor"
                    ]
            }])


The `bond_timeout` parameter must be set to `0`, as this feature is only implemented in the Nav2 nodes. Then, you list the nodes that you want to be managed!


.. code-block:: python
        :linenos:

        return LaunchDescription([call_actor, ride_taxi_actor, lifecycle_manager])


CMake configuration
-------------------

In your ``travel_tutorial`` package, edit the ``CMakeLists.txt`` file to install the ``ride_taxi_actor.py`` script.
Your CMake file must contain the following commands:

.. code-block:: cmake
        :linenos:

        file(GLOB PYTHON_SCRIPTS 
            src/ride_taxi_actor.py
        )
        install(
            PROGRAMS ${PYTHON_SCRIPTS}
            DESTINATION lib/${PROJECT_NAME}
        )


Each time you add a new file, you will have to add a line in the *file* macro, and then rebuild your package:

.. code-block::

    cd ~/oara_ws/
    colcon build


Test the *ride_taxi_actor*
--------------------------

Reproduce the steps seen in the previous tutorial to setup your environment, run a :ref:`list` command to see the status of the deployed modules, and then launch the architecture.
The configuration and activation of the OARA modules should be done automatically by your lifecycle manager node.

Then, before sending a goal to the *ride_taxi_actor*, we will open a subscriber on the *loc* topic to see the incoming message, that is supposed to be sent to the functional layer.

In a new terminal:

.. code-block:: bash

        ros2 topic echo /loc


Then, reuse the :ref:`cli_send_goal` command to send a goal to the
``/ride_taxi_actor`` module, using ``std_msgs/msg/Int32`` types, with a ``'data: 31'`` goal value.
