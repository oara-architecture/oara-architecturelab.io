.. role:: python(code)
  :language: python
  :class: highlight


Replanning on Failure
=====================

In this tutorial, we will improve the *Travel* OARA architecture to manage failure reports: we will **replan** the current goal, at the *Travel Taxi*
level, when the Taxi company is not available.


Interacting with the TAXI call
------------------------------

In the following, we will modify the Travel tutorial launch file to integrate some way to interact with the TAXI call (issued by the `call_taxi` actor).
You are advised to copy the `travel_architecture.launch.py` file into `travel_architecture_simple.launch.py` (for instance) if you want to keep of working version of the first part of the tutorial.


Replacing the `call_taxi` actor with a publisher/subscriber 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the `travel_architecture.launch.py` file, replace the definition of the `call_actor` with the following code:


.. code-block:: python
        :linenos:

        call_actor = LifecycleNode(
                package='oara',
                executable='pub_sub_controller.py',
                name='call_taxi_actor',
                namespace="",
                output="both",
                arguments=["oara_std_interfaces/msg/StringGoal"],
                remappings=[('out', "taxi_call"), ('in', 'taxi_response')]
        )


Instead of printing the goal message (of type `String`) to the terminal, this controller will publish the message on an output topic (here remapped to */taxi_call*), and wait in return to
an acknolwedgement on an input topic (here remapped to */taxi_response*). This ack is of type `std_msgs/msg/Bool`, and if :python:`True`, the goal will terminate successfully. 
If the report is :python:`False`, the goal will be **reformed**.


Interact through the OARA GUI
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In this tutorial, we will install a GUI to interact monitor an OARA architecture, and specifically interact with the `call_taxi_actor` implemented above.

First, go on the colcon workspace and install the `oara_gui` package:

.. code::

        cd ~/oara_ws/src
        git clone --recurse-submodules https://gitlab.com/oara-architecture/oara_gui
        # Install oara_gui dependencies 
        sudo apt install nlohmann-json3-dev libglfw3-dev
        cd ..
        source /opt/ros/humble/setup.bash
        colcon build


You can then run the GUI and launch the architecture:

.. code::

        source ~/oara_ws/install/setup.bash
        ros2 run oara_gui travel_gui


.. code::

        source ~/oara_ws/install/setup.bash
        ros2 launch travel_tutorial travel_architecture.launch.py


In the OARA Gui, you will have 3 widgets, displaying respectively the timelines of the goals managed by each actor (*Timelines Manager* widget), 
that status of each component (*Modules Manager* widget), and a specific layer to interact with the Travel architecture (*Travel Tutorial* widget).
Click on the `/call_taxi_actor` timeline to activate the display. You can also define the initial `loc` and `cash` values in the *Travel Tutorial* widget,
and you should see messages in the terminal from the `travel_observer` indicating that these values have been correctly observed.

.. image:: /_static/images/oara_travel_gui.png
        :align: center
        :width: 1200



Now you can send an goal to the `call_taxi_actor` from the command line: it will publish a message (that we be displayed on the widget), 
and waits for the acknolwedgement, that you will be able to give using the GUI through the **ACCEPT** and **REJECT** buttons:

.. code::

        source ~/oara_ws/install/setup.bash
        ros2 oara send_goal /call_taxi_actor std_msgs/msg/String 'data: "TAXI!"'


If you accept the first goal, then send a new one and reject it, your timeline should look like the following figure.

.. image:: /_static/images/pub-sub-gui.png
        :align: center
        :width: 600


Replanning the Taxi Company
---------------------------

Now that the `call_taxi_actor` can fail in executing the goals, we will have to manager replanning of such failures at a higher
level of the architecture. To do so, we consider in the `travel_taxi` actor, which manages the use of taxis, that we have two
available taxi companies, and therefore we will try to use the second one in case the first one is not available.

The *Travel Taxi* actor
~~~~~~~~~~~~~~~~~~~~~~~

Replace the `travel_taxi_actor.py` script with the following code:

.. code-block:: python
        :linenos:


        #!/usr/bin/env python3
        import enum
        from collections import defaultdict
        from typing import List

        import oara
        import oara.actors

        from oara_std_interfaces.msg import Int32Goal, Float32Goal, StringGoal
        from std_msgs.msg import Int32, String, Float32


        class TaxiCompany(enum.Enum):
            YELLOW = "yellow"
            BLUE = "blue"


        class TravelTaxiActor(oara.actors.POSActor):

            def __init__(self, name: str):
                super().__init__(name, Int32Goal)

                self.add_child_actor('call', StringGoal)
                self.add_child_actor('ride', Int32Goal)
                self.add_child_actor('pay', Float32Goal)
                self.add_data('loc', Int32)

                self.add_parameter(TaxiCompany.YELLOW.value+'-fixed-rate', float, "Fixed rate charged by Yellow Taxis", 1.0)
                self.add_parameter(TaxiCompany.YELLOW.value+'-km-rate', float, "Km rate charged by Yellow Taxis", 1.0)
                self.add_parameter(TaxiCompany.BLUE.value+'-fixed-rate', float, "Fixed rate charged by Blue Taxis", 4.0)
                self.add_parameter(TaxiCompany.BLUE.value+'-km-rate', float, "Km rate charged by Blue Taxis", 0.5)
                self.__selected_taxi : List[oara.UID, TaxiCompany] = defaultdict(list)

            def taxi_rate(self, company: TaxiCompany, distance: int) -> float:
                b = self.get_parameter(company.value+'-fixed-rate').value
                a = self.get_parameter(company.value+'-km-rate').value
                return a * distance + b

            def select(self, gid: oara.UID, goal: Int32):
                return True

            def expand(self, gid: oara.UID, goal: Int32):
                _, loc = self.get_data('loc')
                prices = [(self.taxi_rate(taxi, abs(loc.data - goal.data)), taxi) for taxi in TaxiCompany]
                self.get_logger().info(f"Taxi riding will cost {prices}€")
                min_fare, min_company = min(prices)
                max_fare, max_company = max(prices)

                if min_company not in self.__selected_taxi[gid]:
                    price = min_fare
                    company = min_company
                    self.get_logger().info(f"Calling {min_company.name} Taxi!")
                    self.__selected_taxi[gid].append(min_company)
                elif max_company not in self.__selected_taxi[gid]:
                    price = max_fare
                    company = max_company
                    self.get_logger().info(f"Calling {max_company.name} Taxi!")
                    self.__selected_taxi[gid].append(max_company)
                else:
                    return False

                p = oara.Plan()
                p.add_sequence([
                    (0, String(data=f'{company.name} TAXI!'), 'call'),
                    (1, goal, 'ride'),
                    (2, Float32(data=price), 'pay')
                ])
                p.write("travel-taxi-plan.dot")
                self.add_plan(gid, p, price)
                return True

            def evaluate(self, gid: oara.UID, report: oara.DispatchReport) -> oara.ResolveTo:
                if report.status and report.state == oara.GoalState.FINISHED:
                    return oara.ResolveTo.CONTINUE

                elif report.status and report.actor == "call":
                    return oara.ResolveTo.REPLAN

                else:
                    return oara.ResolveTo.REFORM

        if __name__ == '__main__':
            oara.main(TravelTaxiActor, "travel_taxi_actor")



Let's analyse the changes in this code:

* 
    from line 13 to 16, we define an enumerate structure representing the two available companies: the *Yellow Taxi* company and the *Blue Taxi* company;

* 
    lines 28-32, we declare new parameters for the actor, corresponding to parameters of the function evaluating the cost of the ride, implemented lines 34-37; 
    this function is defined as :math:`cost = fixedRate + x * kmRate`, where :math:`x` is the distance to ride in *km*; 
    the kilometer rate and the fixed rate of each company can be changed in the launch files as standard ROS parameters; 
    the default values give the following functions for the two companies:

    .. image:: /_static/images/travel_cost.png
            :align: center
            :width: 600


    With this configuration, the cheapest company is *Yellow Taxi* for distances lower than 6km, and *Blue Taxi* otherwise.

* 
    the `expand` method then computes the price of each company according to the distance to ride (line 44), and selects the cheapest company that has not 
    yet been selected for this goal (lines 49 and 54); if all companies have been already selected, there is no other solution and the *expansion* fails (line 60);
    otherwise, the plan is computed as in the previous version by decomposing the goal into a sequence of *call*, *ride* and *pay* subgoals (lines 62 to 70);

*
    the `evaluate` method is called when a report is received from one of the children executing a subgoal; the added part of the code is lines 76-77: is we received
    a report from the *call* child, which is not a success (already checked line 73 through the :code:`report.state == oara.GoalState.FINISHED` test),
    then we resolve the goal using a **REPLAN** action, putting back the goal in the *Selected* state.



Testing the replanning loop
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Now that you have made these modifications, you can test them by following the steps below:

* build again your colcon workspace;
* 
    start the *Travel* GUI to monitor the execution of goals: check the timelines of the `/call_taxi_actor`, `/pay_taxi_actor`, `/ride_taxi_actor`, and `/travel_taxi_actor`; 
    also, set the initial values for the `loc` and `cash` states;
* send a goal to the `/travel_taxi_actor` to travel to a destination;
* play with the **ACCEPT** and **REJECT** GUI buttons to test how replanning works.

The following figure shows the timelines of a typical situation where it is requested to travel to destination :math:`15` from location :math:`0`, the first taxi
company (that should be the *Blue Taxis*) rejected the call, then the architecture replans to call the *Yellow Taxis* wich accepts the ride. At the end, the location
is :math:`15` and the remaining cash is :math:`34` over the :math:`100` initially owned.


.. image:: /_static/images/travel_replanning_timeline.png
        :align: center
        :width: 1000
