The *travel_taxi* actor: decompose goals into successive subgoals
=================================================================

The *travel_taxi* actor corresponds to the implementation of the decomposition of the HTN method
**m-travel-by-taxi** shown in the following figure:


.. graphviz:: /_static/images/travel-by-taxi.dot
        :align: center
        :caption: travel-by-taxi method


This actor will not interact with the functional layer, but decomposes the goal it receives
(the location to reach by taxi) into a sequence of subgoals: *call* the taxi, *ride* to
the location, and *pay*.

In this tutorial, we will then see how to implement this actor using a specific actor pattern that 
can manage plans structured as **Partial Order Schedules**. This :ref:`POSActor <oara-planner>` pattern is described in the figure
below:

.. image:: /_static/images/lifecycle-planner.png
        :align: center
        :width: 500


In this pattern, goals must be **Expanded** using a decomposition, based on a specific planner, 
a parametrized decomposition, or hard-coded. Each possible decomposition, or :py:class:`oara_interfaces.msg.Expansion`, is then returned to the parent actor.
Each decomposition must use the :py:class:`oara.Plan` class.
When one of these decomposition is **Dispatched**, the management of the plan execution is automatically done by the actor pattern:
subgoals for which all preceding subgoals have finished can be dispatched; the plan (i.e., the top goal) is then finished when all subgoals are finished.


The *travel_taxi_actor* script
------------------------------

In the *travel_tutorial* package, create a *src/travel_taxi_actor.py* file, and fill it with the following code:

.. code-block:: Python
        :linenos:

        #!/usr/bin/env python3
        import oara
        import oara.actors

        from oara_std_interfaces.msg import Int32Goal, Float32Goal, StringGoal
        from std_msgs.msg import Int32, String, Float32


        class TravelTaxiActor(oara.actors.POSActor):

            def __init__(self, name: str):
                super().__init__(name, Int32Goal)

                self.add_child_actor('call', StringGoal)
                self.add_child_actor('ride', Int32Goal)
                self.add_child_actor('pay', Float32Goal)
                self.add_data('loc', Int32)

            def taxi_rate(self, distance: int) -> float:
                return 1.5 + 0.5 * distance

            def select(self, gid: oara.UID, goal: Int32):
                return True

            def expand(self, gid: oara.UID, goal: Int32):
                _, loc = self.get_data('loc')
                price = self.taxi_rate(abs(loc.data - goal.data))
                self.get_logger().info(f"Taxi riding will cost {price}€")
                p = oara.Plan()
                p.add_sequence([
                    (0, String(data='TAXI!'), 'call'),
                    (1, goal, 'ride'),
                    (2, Float32(data=price), 'pay')
                ])
                p.write("travel-taxi-plan.dot")
                self.add_plan(gid, p, price)
                return True

            def evaluate(self, gid: oara.UID, report: oara.DispatchReport) -> oara.ResolveTo:
                if report.status and report.state == oara.GoalState.FINISHED:
                    return oara.ResolveTo.CONTINUE
                else:
                    return oara.ResolveTo.REFORM

        if __name__ == '__main__':
            oara.main(TravelTaxiActor, "travel_taxi_actor")


This actor is a :py:class:`oara.actors.POSActor`. It manages the execution of a :py:class:`oara.Plan` structure as a partial-order schedule, added in the **expand** method. 
Let's look at some specific code parts:

* lines 14 to 17, we declare three child actors, each one with a name and the kind of goal it can manage; 
  also, we define that we want to access to the *loc* data;

* the ``expand`` method (lines 25 to 37) then decomposes the goal by creating a hard-coded sequence of three tasks; 
  each task is added to the plan as a triplet containing a task index, the associated goal, and the name of the actor that will have to manage this task; 
  then the :py:meth:`oara.actors.Actor.add_plan` method registers this plan for the current goal, and gives a cost value to this plan;

* the ``evaluate`` method (lines 39 to 42) is called each time a report on a sub-goal is received from a child; the ``report`` field contains the child name, 
  whether the sub-goal has been dispatched, and the reported lifecycle state; then we must return how we want to resolve this situation (see the lifecycle state-machine):
  here, in case the sub-goal has correctly finished, we just continue executing the plan; otherwise, we reform the goal (indicating it has failed to execute).

Add the *travel_taxi_actor* to the architecture deployment
----------------------------------------------------------

Edit the ``travel_architecture.launch.py`` file, created in the previous tutorial,
and add this new actor. You must insert the following code in the ``generate_launch_description`` function, and then add the created object in the returned ``LaunchDescription``,
and in the list of managed nodes:

.. code-block:: python
        :linenos:
        
        travel_taxi = LifecycleNode(
            package='travel_tutorial',
            executable='travel_taxi_actor.py',
            name='travel_taxi_actor',
            output="both",
            namespace="",
            parameters=[{
                'loc.observer': 'travel_observer',
                'call.actor': 'call_taxi_actor',
                'pay.actor': 'pay_taxi_actor',
                'ride.actor': 'ride_taxi_actor'
            }])


In addition to the parameter to link the *loc* data with the observer, you have now access to
more parameters to link this actor with its children, using the *call.actor*, *pay.actor* and *ride.actor*
parameters.


Test the *travel_taxi_actor*
----------------------------

Reproduce the steps seen in the previous tutorial to setup your environment and launch the architecture. 
You can send a goal to the *travel_taxi* actor and see how this goal is decomposed and dispatched to the
other actors.