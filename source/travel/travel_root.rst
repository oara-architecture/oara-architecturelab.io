The *travel* actor: HTN planning
================================

The root *travel* actor implements an HTN planning algorithm to solve the general HTN Travel problem.
We define a specific type for the input goals accepted by the *travel* actor, that contains the
*destination* we want to travel to.

We will then first create a specific ``travel_tutorial_interfaces`` package in which we define this type,
and then implement the *travel* actor, that will both plan the way to reach the location using
HTN planning, and replan in case of incoming (unlucky or lucky!) events.

Creation of a specific goal type
--------------------------------

Package Creation
^^^^^^^^^^^^^^^^

First we create the package structure that will contain our goal type definition
and its compilation instructions. The following steps are based on the ROS2 Tutorial on `Creating a package for interface messages <https://docs.ros.org/en/galactic/Tutorials/Beginner-Client-Libraries/Custom-ROS2-Interfaces.html>`_.

.. code-block:: bash

        cd oara_ws/src
        ros2 pkg create travel_tutorial_interfaces --build-type ament_cmake --dependencies oara_interfaces


It creates a folder ``travel_tutorial_interfaces`` with a ``package.xml`` and a ``CMakeLists.txt`` files.


Message Creation
^^^^^^^^^^^^^^^^

First, we will create the ROS structure of the basic goal type. In the *travel_tutorial_interfaces*
package, create a *msg* subfolder, and add there a *TravelDestination.msg* file containing the 
following message definition:

.. code-block:: idl
        :linenos:

        int32 destination
        int32 maximum_walking_distance 2


.. note::

        If you need help in writing your own message type, look at the ROS2 `Interfaces <https://index.ros.org/doc/ros2/Concepts/About-ROS-Interfaces/#interfaceconcept>`_ documentation.


This message contains two fields: the destination location, and the maximum distance we accept to walk to (with a default value of 2).
This message defines the structure of the goals accepted by our the actor we will define later. However, we need to define a second message that combines this structure with
an OARA header in order for the message to be managed by the OARA lifecycle protocol.

Create a *TravelDestinationGoal.msg* file containing the following code:

.. code-block:: idl
        :linenos:

        oara_interfaces/GoalRequest request
        travel_tutorial_interfaces/TravelDestination goal


The field names are imposed by the OARA protocol.


Generating the interface
^^^^^^^^^^^^^^^^^^^^^^^^

Edit the ``CMakeLists.txt`` file to generate the ``TravelDestination`` message. Your ``CMakeLists.txt`` must contain the following lines:

.. code-block:: cmake

        find_package(rosidl_default_generators REQUIRED)
        find_package(oara_interfaces REQUIRED)

        rosidl_generate_interfaces(${PROJECT_NAME}
            "msg/TravelDestination.msg"
            "msg/TravelDestinationGoal.msg"
            DEPENDENCIES oara_interfaces
        )


Your ``package.xml`` file must then contain the following lines:

.. code-block:: xml

        <depend>oara_interfaces</depend>
        <build_depend>rosidl_default_generators</build_depend>
        <exec_depend>rosidl_default_runtime</exec_depend>
        <member_of_group>rosidl_interface_packages</member_of_group>


Compiling the interface
^^^^^^^^^^^^^^^^^^^^^^^

Compile the package using ``colcon``:

.. code-block ::

        cd ~/oara_ws
        colcon build


You can check that your interfaces are correctly generated using the following command:

.. command-output:: ros2 interface package travel_tutorial_interfaces


HTN Planning
------------

In this tutorial, we use a hierarchical planning algorithm, **PyHOP**, a Python version of the famous
HTN planner SHOP2 :cite:`DBLP:journals/jair/NauAIKMWY03`.

Install PyHOP
^^^^^^^^^^^^^

PyHOP must be installed out of your ROS2 workspace, just as any Python library:

.. code::

        git clone https://github.com/lesire/pyhop.git
        cd pyhop
        python3 setup.py install --user


Travel HTN Problem
^^^^^^^^^^^^^^^^^^

To define the HTN problem, create a new ``travel_htn.py`` file in your tutorial package, with the following code:

.. code-block:: python
        :linenos:

        ### Functions ###

        def taxi_rate(x, y):
            dist = abs(y - x)
            return (2 + 1 * dist)

        ### Operators ###

        def walk(state, a, x, y):
            if state.loc[a] == x and abs(y - x) <= state.maximum_walk_distance[a]:
                state.loc[a] = y
                return state
            else:
                return False

        def call_taxi(state, a, x):
            state.loc['taxi'] = x
            return state

        def ride_taxi(state, a, x, y):
            if state.loc['taxi'] == x and state.loc[a] == x:
                state.loc['taxi'] = y
                state.loc[a] = y
                state.owe[a] = taxi_rate(x, y)
                return state
            else:
                return False

        def pay_driver(state, a):
            if state.cash[a] >= state.owe[a]:
                state.cash[a] = state.cash[a] - state.owe[a]
                state.owe[a] = 0
                return state
            else:
                return False

        ### Methods ###

        def travel_by_foot(state, a, x, y):
            return [('walk', a, x, y)]

        def travel_by_taxi(state, a, x, y):
            if state.cash[a] >= taxi_rate(x, y):
                return [('call_taxi', a, x), ('ride_taxi', a, x, y), ('pay_driver', a)]
            return False

        ### Main ###

        if __name__ == "__main__":
            import argparse
            parser = argparse.ArgumentParser()
            parser.add_argument('-l', '--loc', type=int, default=0, help="initial location")
            parser.add_argument('-c', '--cash', type=int, default=10, help="initial cash")
            parser.add_argument('-m', '--maximum-walk-distance', type=int, default=3, help="maximum walking distance")
            parser.add_argument('-d', '--destination', type=int, default=13, help="destination")
            args = parser.parse_args()

            from pyhop import hop
            hop.declare_operators(walk, call_taxi, ride_taxi, pay_driver)
            hop.declare_methods('travel', travel_by_foot, travel_by_taxi)
            hop.print_operators(hop.get_operators())
            hop.print_methods(hop.get_methods())
            s = hop.State('s')
            s.loc = {'me': args.loc, 'taxi': -1}
            s.cash = {'me': args.cash}
            s.owe = {'me': 0}
            s.maximum_walk_distance = {'me': args.maximum_walk_distance}
            print("INITIAL STATE:")
            hop.print_state(s)
            hop.plan(s, [('travel', 'me', args.loc, args.destination)],
                hop.get_operators(), hop.get_methods(),
                verbose=2)


This code defines the following HTN functions, operators and methods:

* ``taxi_rate`` (lines 3 to 5) is just a python function evaluating the price to ride a taxi;
* ``walk``, ``call_taxi``, ``ride_taxi`` and ``pay_driver`` are *operators*; they take as input a current state and some parameters, and must return either ``False`` if the operator cannot be applied, or a new state;
* ``travel_by_foot``, ``travel_by_taxi`` and ``travel_by_foot_desperate`` are methods; they return a list of subtasks into which they are decomposed.

Below, we describe their meaning:

* the *walk* operator, for agent *a* from *x* to *y*, is only possible if *a* is currently at *x* (defined by state value *loc*) and if the walking distance is acceptable; 
  when applied, it changes *a*'s location to *y*;
* the *call_taxi* operator has no preconditions and sets the *taxi* location to *x*;
* the *ride_taxi* operator, for agent *a* from *x* to *y*, is only possible if both *a* and the *taxi* are at *x*; when applied, it changes both *a*'s and the taxi locations to *y*, and sets the state value *owe* by calling the *taxi_rate* function;
* the *pay_driver* operator is only possible if the *cash* value of the state is greater than its *owe* value. When applied, it decreases the *cash* value and sets *owe* to 0;
* the *travel_by_foot* method just decomposes into the *walk* operator;
* the *travel_by_taxi* method is only possible if we have enough *cash*; in that case, the decomposition consists in a sequence of *call_taxi*, *ride_taxi* and *pay_driver*.

This script comes with a *main*, that allows to test the planning model. To test the planner with the initial state being ``{loc: 0, cash: 30}`` and the destination ``15``, run:

.. code-block:: bash

        cd ~/oara_ws/src/travel_tutorial/src
        python3 travel_htn.py -l 0 -c 30 -d 15


The TravelHTN Actor
-------------------

The *travel_htn_actor* script
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In the *travel_tutorial* package, create a *src/travel_htn_actor.py* file, and fill it with the following code:

.. code-block:: Python
        :linenos:

        #!/usr/bin/env python3
        import oara
        import oara.actors

        from oara_std_interfaces.msg import Int32Goal
        from std_msgs.msg import Int32, Float32

        from travel_tutorial_interfaces.msg import TravelDestination, TravelDestinationGoal

        import travel_htn
        from pyhop import hop

        class TravelHTNActor(oara.actors.POSActor):

            def __init__(self, name: str):
                super().__init__(name, TravelDestinationGoal)

                self.add_child_actor('walk', Int32Goal)
                self.add_child_actor('taxi', Int32Goal)
                self.add_data('loc', Int32)
                self.add_data('cash', Float32)

            def on_activate(self, state):
                self.get_logger().info("Creating the HTN problem")
                hop.declare_operators(travel_htn.walk, travel_htn.call_taxi, travel_htn.ride_taxi, travel_htn.pay_driver)
                hop.declare_methods('travel', travel_htn.travel_by_foot, travel_htn.travel_by_taxi)
                return super().on_activate(state)

            def select(self, gid: oara.UID, goal: TravelDestination):
                return True

            def expand(self, gid: oara.UID, goal: TravelDestination):
                _, loc = self.get_data('loc')
                _, cash = self.get_data('cash')
                s = hop.State('s')
                s.loc = {'me': loc.data, 'taxi': -1}
                s.cash = {'me': cash.data}
                s.owe = {'me': 0}
                s.maximum_walk_distance = {'me': goal.maximum_walking_distance}

                htn_solution = hop.plan(s, [('travel', 'me', loc.data, goal.destination)],
                    hop.get_operators(), hop.get_methods(),
                    verbose=0)
                if not htn_solution:
                    self.get_logger().error("No HTN plan found!")
                    return False

                plan = oara.Plan()
                try:
                    ride_goal = next((x[-1] for x in htn_solution if x[0] == 'ride_taxi'))
                    self.get_logger().info(f"solution is to ride to {ride_goal}")
                    plan.add_task(0, Int32(data=ride_goal), 'taxi')
                except StopIteration:
                    try:
                        walk_goal = next((x[-1] for x in htn_solution if x[0] == 'walk'))
                        self.get_logger().info(f"solution is to walk to {walk_goal}")
                        plan.add_task(0, Int32(data=walk_goal), 'walk')
                    except StopIteration:
                        self.get_logger().error("solution contains neither ride nor walk: HTN model error!")
                        return False

                plan.write("travel-htn-plan.dot")
                self.add_plan(gid, plan, 0)
                return True

            def evaluate(self, gid: oara.UID, report: oara.DispatchReport) -> oara.ResolveTo:
                if report.status and report.state == oara.GoalState.FINISHED:
                    return oara.ResolveTo.CONTINUE
                else:
                    return oara.ResolveTo.REFORM


        if __name__ == '__main__':
            oara.main(TravelHTNActor, "travel_htn_actor")


Let's look at this code. Lines 2 to 11, we import the necessary OARA libraries, the interface messages
we have created earlier (``TravelDestination`` goals), as well as the message types for this actor's children,
and its needed data. We then import the PyHOP planner, defined as the ``hop`` object, and the
HTN model we defined earlier in the ``travel_htn`` module.

We then define our ``TravelHTNActor`` actor.
This actor is a :py:class:`oara.actors.POSActor`, just as the ``TravelTaxiActor`` we have defined in the previous tutorial:
yt manages the execution of a :py:class:`oara.Plan`, here limited to one task, being either a destination to walk to or a
destination to go to by taxi.

We then specialize the following methods:

* ``__init__``, to declare the two children *walk* and *taxi*, and the two needed data *loc* and *cash*;
* ``on_activate``, to define the HTN problem by adding the previously defined operators to the *hop* planner, as well as the several methods as possible decompositions for the *travel* task;
* ``select``, that accepts all goals;
* ``expand``, where the HTN-based decomposition takes place:

    * lines 33 to 39, we get the data value and initialize the planning state accordingly;
    * lines 41 to 43, we call the HTN planning algorithm;
    * lines 44 to 46, if there is no plan, we reject the *expand* step;
    * else, the plan should be a flat sequence of actions (and you could see if you test the HTN planning model alone), 
      but we want to send a more abstract subgoal corresponding to the methods (travel by foot or taxi);
      we then check whether a specific action is in the plan:
      
        * lines 50 to 52, the *ride_taxi* action is in the plan, and we decompose the goal with a *taxi* task; 
        * lines 55 to 57, we do the same in case the plan contains a *walk* action; 
        * lines 58 to 60, in case neither a *ride_taxi* nor a *walk* are in the plan (which should not happen actually), we report an expansion error;

    * finally, we add the plan to the possible expansions;

* ``evaluate``, that will be called when the subgoal has terminated, 
  and will then *continue* (in that case, the plan contains only one task, so it just finishes) if the subgoal has correctly finished, or *reform* otherwise.


Build and Test
^^^^^^^^^^^^^^

You should now be familiar with the next steps of this tutorial:

* add this actor to the launch file (take care of defining the data observers and children actors) and the CMakeLists,
* deploy the architecture with ``ros2 launch``
* test the architecture by sending a high-level ``TravelDestination`` goal to the root actor.