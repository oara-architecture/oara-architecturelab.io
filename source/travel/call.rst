The *call* actor: learning command-line tools
=============================================

In this tutorial, we will create a ROS2 launch file to start a :ref:`print_controller`,
and then send goals to it.


Launch file
-----------


OARA does not provide any specific tool to help develop ROS2 Launch files. To learn how to write
launch files, look at the ROS2 `Launch Tutorial <https://index.ros.org/doc/ros2/Tutorials/Launch-Files/Creating-Launch-Files/>`__.

Here we will just write very simple launch files. Create a folder ``launch`` and a 
file ``launch/travel_architecture.launch.py`` in the *travel_tutorial* package, 
and write the following code into this file:

.. code-block:: Python
        :linenos:

        from launch import LaunchDescription
        from launch_ros.actions import LifecycleNode

        def generate_launch_description():

                call_actor = LifecycleNode(package='oara',
                        executable='print_controller.py',
                        name='call_taxi_actor',
                        namespace="",
                        output="both",
                        arguments=["oara_std_interfaces/msg/StringGoal", "--sleep", "5.0"])

                return LaunchDescription([call_actor])


This launch file will deploy the ``print_controller`` module with the given arguments,
name the ROS2 node *call_taxi_actor*, and output its logs to both the standard ouputs and a log file.

To install the launch file, edit the ``CMakeLists.txt`` or your package so that it contains the following command:

.. code-block:: cmake
        :linenos:

        install(DIRECTORY
          launch
          DESTINATION share/${PROJECT_NAME}/
        )


To deploy the architecture, first build your package, then source your workspace and invoke ``ros2 launch``:

.. code-block:: bash

        cd ~/oara_ws
        colcon build
        source install/setup.bash
        ros2 launch travel_tutorial travel_architecture.launch.py


.. warning::

        When you ``colcon build``, colcon installs the Python files (launch files and node scripts)
        by copying them in the install folder; it means that if you modify these files, you would
        have to ``colcon build`` again.

        Colcon provides an option to create symbolic links to Python files, so that you
        can directly relaunch your architecture without building, as it will use the Python files
        from your sources. Take care that it means that any modification made in these files is
        then **directly** taken into account by all the deployments, it then provides to
        let your Python files *under construction* with unfinished modifications.

        To activate these symbolic links, use ``colcon build --symlink-install``

        When you add new files (to the launch folder for instance), you however need to rebuild,
        even with the symlink option.

.. note::

        Each time your want to use packages from your workspace, you must source again the ``setup.bash``
        file. Or you can add ``source ~/oara_ws/install/setup.bash`` into your environment 
        configuration (``~/.bashrc`` for instance)



Introspecting and Configuring from the Command Line
---------------------------------------------------

Once the Travel OARA architecture is launched, you can use the OARA command line tools to interact
with the OARA modules.

First, you can list the deployed modules with the :ref:`list <cli_list>` command in a new terminal:

.. code-block:: bash

        $ source ~/oara_ws/install/setup.bash
        $ ros2 oara list



It reports the ``/call_taxi_actor`` as the only module, and that it is in an *unconfigured* state.
Before using the module, we then have to *configure* and then *activate* it (see :ref:`oara-module-configuration` for details on the configuration states).

We can achieve these steps using the :ref:`lifecycle commands <cli-lifecycle>`:

.. code-block:: bash

        $ ros2 lifecycle set /call_taxi_actor configure
        Transitioning successful

        $ ros2 lifecycle set /call_taxi_actor activate
        Transitioning successful



Send Goals from the Command Line
--------------------------------

Once the *call_taxi_actor* module is active, we can send a goal to it from the command
line, using the :ref:`send_goal <cli_send_goal>` command, that will formulate a new goal to
the actor, and continue on the goal lifecycle until a step fails (see :ref:`oara-goal-lifecycle` for
details on the goal lifecycle steps).

The *call_taxi_actor* actor accepts goals of type *String* (see the parameters in the `Launch file <#Launch-file>`__). We then send the "TAXI!" goal to call a taxi with the following command:

.. code-block:: bash

        ros2 oara send_goal /call_taxi_actor std_msgs/msg/String 'data: "TAXI!"'


The command should display logs (*info* messages only) similar to:

.. code-block:: bash

        [INFO 1644512994.998419572] [oara_send_goal]: connected to actor client /call_taxi_actor
        [INFO 1644512994.999270078] [oara_send_goal]: [actor client /call_taxi_actor] formulating task 40508b8a-5d6f-48ea-9987-04229f17c41e
        [INFO 1644512994.999765592] [oara_send_goal]: Calling 'formulate' with request oara_std_interfaces.msg.String(request=oara_interfaces.msg.GoalRequest(header=std_msgs.msg.Header(stamp=builtin_interfaces.msg.Time(sec=1644512994, nanosec=999341095), frame_id='oara_send_goal'), goal_id='40508b8a-5d6f-48ea-9987-04229f17c41e', plan_id='', step=0), goal=std_msgs.msg.String(data='TAXI!'))
        [INFO 1644512995.380673825] [oara_send_goal]: [actor client /call_taxi_actor] FORMULATED 40508b8a-5d6f-48ea-9987-04229f17c41e
        [INFO 1644512995.381167797] [oara_send_goal]: [actor client /call_taxi_actor] selecting task 40508b8a-5d6f-48ea-9987-04229f17c41e
        [INFO 1644512995.381761411] [oara_send_goal]: Calling 'select' with request oara_std_interfaces.msg.String(request=oara_interfaces.msg.GoalRequest(header=std_msgs.msg.Header(stamp=builtin_interfaces.msg.Time(sec=1644512995, nanosec=381252494), frame_id='oara_send_goal'), goal_id='40508b8a-5d6f-48ea-9987-04229f17c41e', plan_id='', step=1), goal=std_msgs.msg.String(data=''))
        [INFO 1644512996.381516802] [oara_send_goal]: [actor client /call_taxi_actor] SELECTED 40508b8a-5d6f-48ea-9987-04229f17c41e
        [INFO 1644512996.382113460] [oara_send_goal]: [actor client /call_taxi_actor] expanding task 40508b8a-5d6f-48ea-9987-04229f17c41e
        [INFO 1644512996.382856491] [oara_send_goal]: Calling 'expand' with request oara_std_interfaces.msg.String(request=oara_interfaces.msg.GoalRequest(header=std_msgs.msg.Header(stamp=builtin_interfaces.msg.Time(sec=1644512996, nanosec=382226565), frame_id='oara_send_goal'), goal_id='40508b8a-5d6f-48ea-9987-04229f17c41e', plan_id='', step=2), goal=std_msgs.msg.String(data=''))
        [INFO 1644512997.380763717] [oara_send_goal]: [actor client /call_taxi_actor] EXPANDED 40508b8a-5d6f-48ea-9987-04229f17c41e
        [INFO 1644512997.381213458] [oara_send_goal]: [actor client /call_taxi_actor] commiting task 40508b8a-5d6f-48ea-9987-04229f17c41e / plan 43653539-fab7-4543-9ea0-e3bb1dd7541c
        [INFO 1644512997.381743311] [oara_send_goal]: Calling 'commit' with request oara_std_interfaces.msg.String(request=oara_interfaces.msg.GoalRequest(header=std_msgs.msg.Header(stamp=builtin_interfaces.msg.Time(sec=1644512997, nanosec=381287036), frame_id='oara_send_goal'), goal_id='40508b8a-5d6f-48ea-9987-04229f17c41e', plan_id='43653539-fab7-4543-9ea0-e3bb1dd7541c', step=3), goal=std_msgs.msg.String(data=''))
        [INFO 1644512998.380781058] [oara_send_goal]: [actor client /call_taxi_actor] COMMITTED 40508b8a-5d6f-48ea-9987-04229f17c41e
        [INFO 1644512998.381211289] [oara_send_goal]: [actor client /call_taxi_actor] dispatching task 40508b8a-5d6f-48ea-9987-04229f17c41e
        [INFO 1644512998.381727103] [oara_send_goal]: Calling 'dispatch' with request oara_std_interfaces.msg.String(request=oara_interfaces.msg.GoalRequest(header=std_msgs.msg.Header(stamp=builtin_interfaces.msg.Time(sec=1644512998, nanosec=381284503), frame_id='oara_send_goal'), goal_id='40508b8a-5d6f-48ea-9987-04229f17c41e', plan_id='', step=4), goal=std_msgs.msg.String(data=''))
        [INFO 1644512999.381183661] [oara_send_goal]: [actor client /call_taxi_actor] DISPATCHED 40508b8a-5d6f-48ea-9987-04229f17c41e
        [INFO 1644513005.379911276] [oara_send_goal]: FINISHED oara_interfaces.msg.GoalState(header=std_msgs.msg.Header(stamp=builtin_interfaces.msg.Time(sec=1644513005, nanosec=378886846), frame_id='call_taxi_actor'), expansions=[], plan_id='', goal_id='40508b8a-5d6f-48ea-9987-04229f17c41e', state=6)




Note the 5 seconds between the *DISPATCHED* message and the *FINISHED* message: they correspond to the execution duration of the goal by the actor.