The *travel* observer: provide data to actors
=============================================

The other actors we will implement, that manage *paying* the taxi or *walking* to the destination,
need some information about the current state of your system-under-control in order to control this system.
Concretely, the *walk* actor will need to know where we are (at which *location*),
and the *pay* actor will need to know how much *cash* we have.

In the OARA concepts, the Actors never directly get data from the functional layer. This data is instead gathered
by **Observers** and stored internally so that actors can access to these data consistently.

In this tutorial, we will design an observer that:

* provides the *loc* and *cash* data to the OARA *actors*,
* update the internal values of these data from the functional layer.

The *travel_observer* script
----------------------------

In the *travel_tutorial* package, create a *src/travel_observer.py* file, and fill it with the following code:

.. code-block:: Python
        :linenos:

        #!/usr/bin/env python3
        import oara
        import oara.observers

        from std_msgs.msg import Int32, Float32


        class TravelObserver(oara.observers.Observer):

            def __init__(self, name: str):
                super().__init__(name)
                self.add_data('loc', Int32)
                self.add_data('cash', Float32)
                self.add_parameter('init_loc', oara.Parameter.Type.INTEGER, 
                                    'initial location', 0)
                self.add_parameter('init_cash', oara.Parameter.Type.DOUBLE, 
                                    'initial amount of cash', 100.0)

            def on_configure(self, state):
                init_loc = self.get_parameter('init_loc').value
                self.get_logger().info(f"Initial location: {init_loc}")
                self.set_data('loc', Int32(data=init_loc))
                init_cash = self.get_parameter('init_cash').value
                self.get_logger().info(f"Initial cash: {init_cash}")
                self.set_data('cash', Float32(data=init_cash))
                return super().on_configure(state)

            def on_activate(self, state):
                self.get_logger().info("Creating ROS2 subscribers")
                self.loc_sub = self.create_subscription(Int32, "loc", self.loc_cb, 1)
                self.cash_sub = self.create_subscription(Float32, "cash", self.cash_cb, 1)
                return super().on_activate(state)

            def on_deactivate(self, state):
                self.destroy_subscription(self.loc_sub)
                self.destroy_subscription(self.cash_sub)
                return super().on_deactivate(state)

            def loc_cb(self, msg):
                self.get_logger().info(f"At location {msg.data}")
                self.set_data('loc', msg)

            def cash_cb(self, msg):
                self.get_logger().info(f"I have {msg.data}€")
                self.set_data('cash', msg)


        if __name__ == '__main__':
            oara.main(TravelObserver, "travel_observer")


Lines 1 to 3 are similar to the previous tutorial: we set the shebang and import *oara* libraries.

Line 5, we import the ROS2 messages corresponding to the managed data: `Int32` for the location, `Float32` for the money.

We then define our ``TravelObserver`` (from line 8):

* at initialization (lines 10 to 17), we define the two provided data using the :py:meth:`oara.observers.Observer.add_data` method, 
  and we also define two ROS2 parameters that will allow to set the initial values of these data, in case nothing is received from the functional layer; 
  this is done using the :py:meth:`oara.internal.module.Module.add_parameter` method;

* on configuration (lines 19 to 26), we get the values of these parameters (using :py:meth:`rclpy.node.Node.get_parameter`) and set the internal data accordingly;

* on activation (lines 28 to 32), we create two ROS2 subscribers to get the data values from the functional layer; 
  each subscriber is bound to a callback method that updates the corresponding data value (lines 39 and 43), using the :py:meth:`oara.observers.Observer.set_data` method;

* from line 34 to 37, we define what to do on *deactivation*, i.e. if we want to put back the node in an unconfigured state: 
  we remove the subscriptions, to be sure that we won't react to new data while we are not activated again.


.. note::

        For more information on ROS2 subscribers definitions and parameters, look at the
        `Python Publisher/Subscriber Tutorial <https://index.ros.org/doc/ros2/Tutorials/Writing-A-Simple-Py-Publisher-And-Subscriber/>`__.


Add the *travel_observer* to the architecture deployment
--------------------------------------------------------

Edit the ``travel_architecture.launch.py`` file, created in the previous tutorial,
and add this new observer. You must insert the following code in the ``generate_launch_description`` function, and then add the created module in the ``lifecycle_managerr`` managed nodes, and in the returned ``LaunchDescription``:

.. code-block:: python
        :linenos:

        travel_observer = LifecycleNode(
            package='travel_tutorial',
            executable='travel_observer.py',
            name='travel_observer',
            namespace='',
            output="both",
            parameters=[{'init_cash': 50.0}])


Note that we use the *parameters* field to initialize some parameter values!

.. warning::

    Do not forget to add the `travel_observer.py` file to your CMake file, make your script executable, and to build your workspace again!



Test the *travel_observer*
--------------------------

Reproduce the steps seen in the previous tutorial to setup your environment and launch the architecture. You can then interact with the observer using standard ROS2 command line tools.

Ride the taxi
^^^^^^^^^^^^^

Send a goal to ride the taxi, has done in the previous tutorial, using the following command:

.. code::

    ros2 oara send_goal /ride_taxi_actor std_msgs/Int32 "data: 31"

You should then see in the log messages of your architecture that, when the goal is achieved, the `travel_observer`
indicates that the `loc` data has been updated:

.. code::

    [ride_taxi_actor.py-2] [INFO 1644563843.613364701] [ride_taxi_actor]: Goal c195dc3a-7ddb-4a05-80bc-9867d8913b49 FINISHED
    [travel_observer.py-3] [INFO 1644563843.614569953] [travel_observer]: At location 31


You can then check the `loc` data value owned by the observer using the :ref:`get_data <cli_get_data>` command:

.. code::

    ros2 oara get_data /travel_observer loc


Get the money
^^^^^^^^^^^^^

Simulate that you receive a new amount of money from the functional layer with the following command:

.. code::

    ros2 topic pub /cash std_msgs/msg/Float32 "data: 72.5" --once


You should then see again in the log messages that the `cash` data value has been updated:

.. code::

    [travel_observer.py-3] [INFO 1644566404.513054291] [travel_observer]: I have 72.5€


and can then check the value owned by the observer:

.. code::

    ros2 oara get_data /travel_observer cash


