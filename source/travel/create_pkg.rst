Create the *travel_tutorial* package
====================================

We will first create a package that will contain the OARA modules and associated
scripts for the *Travel Architecture*.

To create an empty package, we use the standard ROS command ``ros2 pkg create``. 
The options of this command are reported below:

.. command-output:: ros2 pkg create --help


To create the *travel_tutorial* package, use the ``pkg create`` with the following options (assuming you want to create it in you `oara_ws` workspace):

.. code-block:: bash

        ros2 pkg create travel_tutorial \
                        --description "OARA travel tutorial package" \
                        --destination-directory ~/oara_ws/src/ \
                        --build-type ament_cmake \
                        --dependencies oara oara_interfaces oara_std_interfaces
