The *pay* actor: get data and check goal values
===============================================

In this tutorial, we will create an **Actor** that will manage to *pay* taxis. 
Similarly to the *ride* actor, the *pay* actor is a **Controller**: it does not perform any goal expansion, it just dispatches the goal it receives. 
:ref:`Controllers <oara-controller>` implement a partial Goal lifecycle.

The **pay** actor will accept goals corresponding to the price to pay for the ride, represented by a Float value. 
It will get the current cash available, check that we have enough to pay(!) and interact with the functional layer to
make the paiement.


The *pay_taxi_actor* script
---------------------------

In the *travel_tutorial* package, create a *src/pay_taxi_actor.py* file, and fill it with the following code:

.. code-block:: Python
        :linenos:

        #!/usr/bin/env python3
        import oara
        import oara.actors

        from std_msgs.msg import Float32
        from oara_std_interfaces.msg import Float32Goal

        class PayTaxiActor(oara.actors.Controller):

            def __init__(self, name: str):
                super().__init__(name, Float32Goal)
                self.add_data('cash', Float32)

            def on_activate(self, state):
                self.get_logger().info("Creating 'cash' publisher")
                self.cash_pub = self.create_publisher(Float32, "cash", 1)
                return super().on_activate(state)

            def on_deactivate(self, state):
                self.destroy_publisher(self.cash_pub)
                return super().on_deactivate(state)

            def select(self, goal_id, goal):
                _, cash = self.get_data('cash')
                self.get_logger().info(f"I have {cash.data}€")
                return cash.data >= goal.data

            def dispatch(self, goal_id, goal, plan):
                _, cash = self.get_data('cash')
                remaining = cash.data - goal.data
                self.cash_pub.publish(Float32(data=remaining))
                self.get_logger().info(f"Taxi paid; remaining cash: {remaining}")
                return True

            def monitor(self, goal_id):
                self.finish(goal_id)

            def drop(self, goal_id):
                return True

        if __name__ == '__main__':
            oara.main(PayTaxiActor, "pay_taxi_actor")


You should now be familiar with the first lines of this script: it imports the *oara* libraries, and the message types corresponding to the accepted goals (*Float32*).

The *pay_taxi_actor* then has a specific implementation for the following methods:

* on initialization, it declares that it needs to access to data *cash* (line 12);
* on activation, it creates a ROS2 publisher to send the remaining cash value to the functional layer (line 16); this publisher is destroyed in case of deactivation (line 20);
* on selection, it gets the amount of cash from the observer, and checks that we have enough to pay the taxi (lines 23 to 26); otherwise, *selection* of the goal is rejected (and it's up to the parent actor to decide what to do); the ``get_data`` method returns a pair containing a header and the data value, hence the ``_``.
* on dispatch, it sends the remaining cash value to the functional layer through the publisher (line 31);
* monitoring directly sets the goal as finished (line 36); consequently, *drop* is void.


Add the *pay_taxi_actor* to the architecture deployment
-------------------------------------------------------

When declaring a new data in the actor code, we do not indicate which observer will provide this data. 
This point is of high importance to respect the Separation of Concerns :cite:`radestock1996`. 
It is then at the deployment time that we bind the observer that
provides the data to the actors that require the data, through a parameter named
*<data>.observer* (here ``cash.observer`` for our *cash* data).

Edit the ``travel_architecture.launch.py`` file, created in the previous tutorial,
and add this new actor. You must insert the following code in the ``generate_launch_description`` function, and then add the created object in the returned ``LaunchDescription`` list, and in the *managed* nodes:

.. code-block:: python
        :linenos:

        pay_taxi_actor = LifecycleNode(
            package='travel_tutorial',
            executable='pay_taxi_actor.py',
            name='pay_taxi_actor',
            namespace='',
            output="both",
            parameters=[{'cash.observer': 'travel_observer'}])




Test the *pay_taxi_actor*
--------------------------

Reproduce the steps seen in the previous tutorial to setup your environment and launch the architecture. 
In this part, try to perform the following steps by yourself to test the *pay* actor:

* setup the current *cash* amount to 50€ by publishing to the `/cash` topic;

* send a goal to the *pay_taxi_actor* actor, to pay a taxi fare of 30€; the actor should accept to select and then execute this goal;

* send a new goal to the *pay_taxi_actor* actor, to pay again a taxi fare of 30€: the actor should refuse to select this goal!
