Replanning on Events
====================

In this tutorial, we will define an *Event Monitor* in the observer, in order to trigger an event when the `cash` value
raises above a given threshold. Then we will subscribe to this event from our *Travel HTN* actor in order to replan the current
goal when new cash is found.

Monitoring the discovery of *Cash*
----------------------------------

Event monitors are based on Past-Time Metric Temporal Logic :cite:`DBLP:journals/corr/abs-1901-00175`, using the `Reelay <https://doganulus.github.io/reelay/>`__
library. It allows to write complex formula evaluating the evolution of the values of some data. The formulas are written in the code using
a `string` expression. The main constraint is that the formula must only use variables which are *data* of the actual observer monitoring the event.

Monitor formulas are evaluated each time the data value changes.

Adding an event monitor in an observer is done in its constructor, just like when we add managed data or parameters.
Modify the code of the `travel_observer.py` script to have the constructor be like:

.. code-block:: Python
        :linenos:

            def __init__(self, name: str):
                super().__init__(name)
                self.add_data('loc', Int32)
                self.add_data('cash', Float32)
                self.add_parameter('init_loc', oara.Parameter.Type.INTEGER,
                                    'initial location', 0)
                self.add_parameter('init_cash', oara.Parameter.Type.DOUBLE,
                                    'initial amount of cash', 100.0)
                self.add_data_monitor('cash_found', 'cash', 
                                      r"pre{cash < 100} and {cash >= 100}")


The event monitor is added lines 9 and 10. The method :py:meth:`oara.observers.Observer.add_data_monitor` takes three arguments:

* the first one is the name you want to give to the event, here *cash_found*;
* 
        the second is the *data* managed by your observer used in your formula; 
        in case your formula uses several data, this argument corresponds to the data that will trigger the formula evaluation;
        here, the data used in the formula is *cash*;
*
        the third argument is the formula specification; 
        its meaning is that our *cash_found* event will be true when the property :math:`cash < 100` was true at the preceeding timestep, and the property :math:`cash >= 100` is currently true;
        more clearly: *we had less than 100€ and now we have more!*

In the current OARA version, events are emitted by observers only when their formula evaluates to :code:`True`.


If you want to test the event emission alone, you can launch the architecture, start the Travel GUI, and change the `cash` value to activate the event!

Replanning when more cash available
-----------------------------------

Now, we will implement a reaction to this event in the top actor of our architecture, *Travel HTN*. The idea is that, knowing that we have more than `100` euros,
we may pay a taxi whereas we had to walk with less money. The implementation of this reaction will be made with a lot of simplifications:

* we will systematically replan the current goal, even if it was already using a taxi to travel
* we will consider that all the subgoals can be interrupted, even if we already rode to the destination and still have to pay the taxi

Removing these assumptions would lead to much more code to implement, and goes further the basic OARA API.


The reactions to events correspond to the same transitions of the Goal Lifecycle as the ones of subgoals failure management: each goal can be resolved to a previous
state of its lifecycle. The main difference is that an event is not bound to a specific goal by nature. The proposed default stategies can then be applied to either
all the goals currently managed by the actor, or to the goals that are currently dispatched. Any other strategy would need to be reimplemeted in the actor subclass.

The strategies provided by the OARA API are defined in the :py:class:`oara.internal.event.EventReaction` structure.

In order to implement such an event reaction in the *Travel HTN* actor, you must replace the constructor in the `travel_htn_actor.py` file by the following code:

.. code-block:: Python
        :linenos:

            def __init__(self, name: str):
                super().__init__(name, TravelDestinationGoal)
                self.add_child_actor('walk', Int32Goal)
                self.add_child_actor('taxi', Int32Goal)
                self.add_data('loc', Int32)
                self.add_data('cash', Float32)
                self.add_event("cash_found", oara.EventReaction.REPLAN)


The only change is line 7. We declare that we want to subscribe to a new event, named *cash_found*, and the strategy we want to apply is **REPLAN**: all the goals
known by the *Travel HTN Actor* will be resolved to the *SELECTED* state and then dispatched again, if possible.

Similarly to the *data*, declared *events* will lead to the definition of parameters that you must set in your launch file to bind the event with the observer
that will emit it. Change the declaration of the *Travel HTN* actor in you launch file with the following code:


.. code-block:: Python
        :linenos:

            travel_htn = LifecycleNode(
                package='travel_tutorial',
                executable='travel_htn_actor.py',
                name='travel_htn_actor',
                output="both",
                namespace="",
                parameters=[{
                    'loc.observer': 'travel_observer',
                    'cash.observer': 'travel_observer',
                    'walk.actor': 'walk_actor',
                    'taxi.actor': 'travel_taxi_actor',
                    'cash_found.observer': 'travel_observer'
                }])


And that's all!!

Now, you can test your new replanning architecture. A typical scenario would be:

* start the GUI and launch your architecture, initialize `cash` with some money;
* if you have more than `100€`, travel to a few destination to spend some money, until you are below `100€`; you will have to accept the Taxi *call* requests from the GUI;
* send a new goal to the *Travel HTN* actor; before accepting the Taxi call request, change the value of `cash` in the GUI to put it above `100€`;
* the *Travel HTN* actor should react to it, drop the current goal decomposition, and replan a new decomposition!
