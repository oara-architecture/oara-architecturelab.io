The *walk* actor: durative execution control
============================================

Here we define the last *Controller* of the travel architecture. Whereas the previous
controllers we defined (*ride* and *pay*) executed instantaneously the goals (calling *finish* at the first call of *monitor*), the *walk* actor will take some time... depending on the distance to walk!


The *walk_actor* script
-----------------------

In the *travel_tutorial* package, create a *src/walk_actor.py* file, and fill it with the following code:

.. code-block:: Python
        :linenos:

        #!/usr/bin/env python3
        import oara
        import oara.actors

        from std_msgs.msg import Int32
        from oara_std_interfaces.msg import Int32Goal


        class WalkActor(oara.actors.Controller):

            def __init__(self, name: str):
                super().__init__(name, Int32Goal)
                self.add_data('loc', Int32)

            def on_activate(self, state):
                self.get_logger().info("Creating 'loc' publisher")
                self.loc_pub = self.create_publisher(Int32, "loc", 1)
                return super().on_activate(state)

            def on_deactivate(self, state):
                self.destroy_publisher(self.loc_pub)
                return super().on_deactivate(state)

            def select(self, goal_id, goal):
                return True

            def dispatch(self, goal_id, goal, plan):
                header, value = self.get_data('loc')
                self.__loc = value.data
                self.get_logger().info(f"walking from {self.__loc} to {goal.data}")
                return True

            def monitor(self, goal_id):
                goal = self.get_goal(goal_id)
                self.get_logger().debug(f"current loc: {self.__loc}")
                distance = goal.data - self.__loc
                if distance == 0:
                    self.get_logger().info(f"Arrived at location {goal}")
                    self.finish(goal_id)
                else:
                    if distance > 0:
                        self.get_logger().info(f"one step forward...")
                        self.__loc += 1
                    else:
                        self.get_logger().info(f"one step backward...")
                        self.__loc -= 1
                    self.loc_pub.publish(Int32(data=self.__loc))

            def drop(self, goal_id):
                return True


        if __name__ == '__main__':
            oara.main(WalkActor, "walk_actor")


Regarding the previous actors we implemented, the *walk_actor* specificities relies on the fact
that the dispatching of the goal lasts a certain time! 
The *monitoring*, that is called periodically, will take care of the progress of the walk. 
Let's look at it in details:

* lines 1 to 6, we find the now usual imports
* lines 11 to 13, we define a new Controller taking *Int32* as goals, with an input data of type *Int32*;
* lines 15 to 18, on activation, we create the publisher to publish the new location;
* lines 27 to 31, on dispatch, we get the current location *data* and store it locally;
* in the ``monitor`` function:

  * we first compute the distance between the current location and the goal (line 36);
  * if arrived, the goal execution is finished (lines 37 to 39);
  * otherwise, we increment or decrement the current location, walking one step towards the goal, and publish this new location (line 47)


Add the *walk_actor* to the architecture deployment
---------------------------------------------------

Edit the ``travel_architecture.launch.py`` file, created in the previous tutorial,
and add this new actor. You must insert the following code in the ``generate_launch_description`` function, and then add the created object in the returned ``LaunchDescription`` list, and in the managed node list:

.. code-block:: python
        :linenos:
        
        walk_actor = LifecycleNode(
            package='travel_tutorial',
            executable='walk_actor.py',
            name='walk_actor',
            output="both",
            namespace="",
            parameters=[{'loc.observer': 'travel_observer'}])



Test the *walk_actor*
---------------------

Reproduce the steps seen in the previous tutorial to setup your environment and launch the architecture. 
In this part, try to perform the following steps by yourself to test the *walk*
actor:

* look at the current location with the *get_data* command-line interface;

* send a goal to the *walk_actor* node, far enough to have time to see you walking :)

* look at the location in the *travel_observer*, it should evolve as the *walk* actor executes the goal;

* also, you can use a ``ros2 topic echo /loc`` to see the *commands* sent to the functional layer.

