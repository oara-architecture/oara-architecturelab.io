:tocdepth: 3

OARA -- ONERA's Architecture for Robot Autonomy
===============================================

OARA is a toolchain to develop decision-making architectures for
autonomous robots.


OARA is based on `ROS2 <https://index.ros.org/doc/ros2/>`__.
The current ROS2 supported version is **Humble**.
The OARA ROS2 toolchain contains an API to define decision-making modules, common Goal description messages, and tools to introspect, display, interact with these modules.

.. toctree::
   :maxdepth: 1

   quick/index
   travel/index
   oara/index
   cli/index
   API <api/index>
   refs


.. todo::

  Show an example and explain briefly the concepts.




Sister projects
===============

Robot Skills Language and Tools
-------------------------------

A formalization of robot skills using a specific language,
and tools to generate ROS/ROS2 skill manager architectures, interface libraries,
and models for decision-making and verification.

https://onera-robot-skills.gitlab.io/


Cite this work
==============

If you use OARA, please cite :cite:`lesire2022hierarchical` using:

.. code-block:: bibtex
  
   @inproceedings{lesire2022hierarchical,
     title = {A Hierarchical Deliberative Architecture Framework based on Goal Decomposition},
     author = {Lesire, Charles and Bailon-Ruiz, Rafael and Barbier, Magali and Grand, Christophe},
     booktitle={2022 IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS)}, 
     address = {Kyoto, Japan},
      year = {2022},
     month = {Oct},
     doi={10.1109/IROS47612.2022.9981488}
   }

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
