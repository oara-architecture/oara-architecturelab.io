.. _oara-goal-lifecycle:

Actors and Goal Lifecycle
=========================

Actors
------

The modules that manage goals and their execution are called **Actors** in OARA. In Actors, 
the management of incoming goals is described by a **Goal Lifecycle**.

Goal Lifecycle
--------------

Each goal received by an actor has a lifecycle described by the following figure:

.. image:: /_static/images/lifecycle-actor.png
        :align: center
        :width: 500

This lifecycle management has been taken from :cite:`actorsim`, and leveraged to 
the multi-actor architecture concept of OARA. 
This figure uses the following style convention:

* plain arrows represent requests that can be made by a parent actor;
* dashed arrows represent either internal transitions triggered either by function calls, or, for the specific case of the **Process**, by an incoming event;
* italic caps transitions are available through function call for the user interface;
* red-filled transitions are actor steps that must be specialized.

The states/steps of this lifecycle are described hereafter:

* a parent actor can send a new goal to this actor using the **Formulate** request, putting then the goal in its initial *Formulated* state;
* the *selection* step consists in activating the goal; it is triggered by the **Select** request, and the acceptance of this step can be specialized to test the value of the goal for instance;
* the *expansion* step consists in computing the decomposition of the goal into sub-goals; it has to be specialized, and can use a planning algorithm, a set of pre-computed plan, or a hard-coded decomposition; several decompositions/plans can be returned to the parent actor;
* the *commitment* step consists in specifying which decomposition has been choosed by the parent actor;
* then, the *dispatching* consists in actually executing the goal by sending subgoals to children actors; a **Monitor** function checks for the execution of subgoals, and then calls either the **Finish** transition if the goal has been successfully executed, or the *Evaluation* transition to evaluate the status of the goal or a subgoal;
* on *evaluation*, several actions can be triggered (generally called *goal resolving*): 
  
  * **Continue** with the next subgoal;
  * **Repair** to automatically activate another computed decomposition to execute this goal;
  * **Replan** to recompute a new decomposition to execute this goal;
  * **Defer** if the selection check has to be performed again;
  * **Reform** if the goal cannot be executed anymore in the current situation.

The actor can also accept a **Drop** request, in which case it will remove the goal from the local
actor memory, and drop subgoals if any. Also, incoming events to which the actor has subscribed can
trigger the **Process** function, that can in turn drop, or resolve some managed goals.
