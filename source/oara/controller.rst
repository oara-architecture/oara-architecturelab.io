.. _oara-controller:

Controllers
===========

A **Controller** is a specific kind of **Actor** that does not decomposes the goal into subgoals but generally just *executes* it.


Managed Goal Lifecycle
----------------------

Regarding the managed **Goal Lifecycle**, the *Controller* pattern can be described by the following figure:

.. image:: /_static/images/lifecycle-controller.png
        :align: center
        :width: 500


The lifecycle steps to reimplement are:

* :py:meth:`oara.actors.Actor.select`
* :py:meth:`oara.actors.Actor.drop`
* :py:meth:`oara.actors.Actor.dispatch`
* :py:meth:`oara.actors.Actor.monitor`


Specifically, on *dispatch*, it is the responsibility of the controller developper to
implement the link with its own interface or functional layer, and to manage the reception
of the execution results.

When the goal actual execution is ended, the controller can use either:

* :py:meth:`oara.actors.Actor.finish` to indicate that the goal is successful
* :py:meth:`oara.actors.Actor.reform` to indicate that the goal has failed, and place the goal in the *Formulated* state


Sequence Diagrams
-----------------

We describe typical behaviors of the **Controller** through three sequence diagrams.
In all diagrams:

- red labels are methods that *must* be reimplemented by the controlle developer,
- dark blue labels are methods available for the user to *notify* the end of execution or a specific resolving strategy
- light blue are application specific behaviours


Successful goal execution
^^^^^^^^^^^^^^^^^^^^^^^^^

On the following diagram, we suppose an parent *Actor* that sends a goal to our **Controller**
module, that will control the actual execution in relation with a *Functional Layer*.


.. image:: /_static/images/sd-controller.png
        :align: center
        :width: 500


The *Actor* goes through the goal lifecycle. In red, are mentioned the methods that must 
be specialized in a controller: **select** and **dispatch**.
During **dispatch**, the developer has to program the link with the functional layer to start
the goal execution. It can consists in using a ROS2 interface, like
`Topics <https://index.ros.org/doc/ros2/Tutorials/Topics/Understanding-ROS2-Topics/>`__ or 
`Services <https://index.ros.org/doc/ros2/Tutorials/Services/Understanding-ROS2-Services/>`__, 
or a specific protocol, like a direct Python library, or a message passing protocol, like 
`0MQ <https://zeromq.org/>`__ or `Protobuf <https://developers.google.com/protocol-buffers>`__.

The **dispatch** function must return ``True`` when the execution is started. The end of the 
execution is controlled in the **monitor** method. In this method, you can either check a local 
variable you would have received from a callback, or directly ask the functional layer for the
goal status. When the execution is successfully finished, call the **finish** method to
change the goal state!


Periodic goal monitoring
^^^^^^^^^^^^^^^^^^^^^^^^

In controllers, the monitoring function is actually periodically called to check for the
execution status of the goal. The default period is 1 second, but can be changed by defining the ``oara.period`` parameter of the node.
The general periodic behaviour is depicted on the following diagram:

.. image:: /_static/images/sd-controller-periodic.png
        :align: center
        :width: 500


Execution failure
^^^^^^^^^^^^^^^^^

When the functional layer reports a failure of the execution, the only available option on a
**Controller** pattern is to **reform** the goal. The result (i.e., the goal being back in
the *Formulated* state) will be reported to the parent actor; this actor can then decide either
to **drop** the goal (which is the case in the diagram below), or to retry an execution
later.

.. image:: /_static/images/sd-controller-reform.png
        :align: center
        :width: 500

