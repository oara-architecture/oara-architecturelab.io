OARA Concepts
=============

.. toctree::
   :maxdepth: 2

   module_configuration
   goal_lifecycle

   controller
   planner
