.. _oara-planner:

POS Actor
=========

A **POS** actor is a specific kind of **Actor** that focuses on planning (i.e. *expanding* goals),
that manages plans structured as *Partial-Order Schedules*.


Managed Goal Lifecycle
----------------------

Regarding the managed **Goal Lifecycle**, the *POS* pattern can be described by the following figure:

.. image:: /_static/images/lifecycle-planner.png
        :align: center
        :width: 500

The lifecycle steps to reimplement are:

* :py:meth:`oara.actors.Actor.select`;
* :py:meth:`oara.actors.Actor.expand`, that decomposes the goal into a plan of tasks;
* :py:meth:`oara.actors.Actor.evaluate`, that will analyse the output of a task execution to determine if we must *continue*, *replan* or *reform*.

Incoming event management, through the **Process** call, are generally based on reaction strategies,
and most of the time must not be reimplemented.


Sequence Diagrams
-----------------

We describe typical behaviors of the **POS** pattern through three sequence diagrams.
In all diagrams:

- red labels are methods that *must* be reimplemented by the controlle developer,
- dark blue labels are methods available for the user to *notify* the end of execution or a specific resolving strategy
- light blue are application specific behaviours


POS Actor with a Unique subgoal
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

On the following diagram, we present a sequence involving a **POS** actor managing a plan that contains a
*unique* subgoal, i.e. the decomposition of a goal consists in one unique subgoal to be executed.


.. image:: /_static/images/sd-planner-unique.png
        :align: center
        :width: 500


The *Actor* goes through the goal lifecycle. In red, are mentioned the methods that must 
be specialized in the *Planner* actor: **select** and **expand**.
During **dispatch**, the *Unique* dispatcher takes care of going through the lifecycle of the subgoal.
If the subgoal is correctly executed, which is checked in the **evaluate** step, the goal is *finished*.


Replanning
^^^^^^^^^^

The following diagram shows a situation where the call to the first **evaluate** asks for a
goal replanning. In that case, the subgoals are *dropped*, and the **expand** step is called again.
In the expand succeeds (i.e., finds a new decomposition), then the new plan is dispatched.

.. image:: /_static/images/sd-planner-replan.png
        :align: center
        :width: 500

