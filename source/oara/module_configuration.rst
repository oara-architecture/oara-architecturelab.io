.. _oara-module-configuration:

Module Configuration
====================

All OARA modules (actors, observers) implement the ROS2 `Node Lifecycle <https://design.ros2.org/articles/node_lifecycle.html>`__ interface.
You can find below the state machine implemented in ROS2 managed nodes:

.. image:: /_static/images/life_cycle_sm.png
        :align: center
        :width: 600

        
For each transition of this state machine, it is possible to redefine callbacks. The OARA modules (actors, observers) already redefine
these callbacks to manage internal structures, create ROS2 communication channels to communicate between modules, ...

The transitions can finally be triggered using the OARA :ref:`lifecycle commands <cli-lifecycle>`.

In this page, you will find some advices on what to put on the several callbacks if you need to redefine them.


Initialization
--------------

When you define your own module, you should put in the constructor of your class the following elements:
* declaration of new parameters
* declaration of provided data (for *observers*)
* declaration of required data (for *actors*) that do not rely on parameter values
* declaration of clients (for *actors*) that do not rely on parameter values


Configuration
-------------

You can specialize in your module the :py:meth:`oara.internal.module.Module.on_configure` method.
On configuration, it is recommended to do the following specific actions:

* get and check parameter values
* check availability or soundness of specific data (like a planning model)

Here is an example of specialization of the :py:meth:`oara.internal.module.Module.on_configure` method:

.. code-block:: python

    def on_configure(self, state):
        init_loc = self.get_parameter('init_loc').value
        self.set_data('loc', Int32(data=init_loc))
        self.get_logger().info(f"Initial location: {init_loc}")
        return super().on_configure(state)

Your callbacks must always accept a *state* parameter, that contains the current node state. It is also advised
to call the configuration of the base class at the end. Each callback must return one of the :py:class:`oara.internal.CallbackReturn` values.

Cleanup
-------

In the cleanup callback, you should undo the critical elements done during configuration to allow for
a new configuration of your module.

Activation
----------

You can specialize in your module the :py:meth:`oara.internal.module.Module.on_activate` method.
Activation is the part where the real execution will start; it is the place to:

* create specific communication channels to your functional layer (for *controlers*)
* send initial data to the functional layer

Here is an example of specialization of the :py:meth:`oara.internal.module.Module.on_activate` method:

.. code-block:: python

    def on_activate(self, state):
        self.get_logger().info("Creating 'cash' publisher")
        self.cash_pub = self.create_publisher(Float32, "cash", 1)
        return super().on_activate(state)

Deactivation
------------

In the deactivation callback, you should undo the elements of the activation, i.e. properly closing
connection with a functional layer, or access to a file, ... in order to allow for a further new activation of the module.
