OARA Observers
==============

Usefull Methods
---------------

.. rubric:: Creation

.. autosummary::

        oara.observers.Observer


.. rubric:: Data Management

.. autosummary::

        oara.observers.Observer.add_data
        oara.observers.Observer.set_data
        oara.observers.Observer.get_data
        oara.observers.Observer.add_function


.. rubric:: Event Management

.. autosummary::

        oara.observers.Observer.add_data_monitor


Observer Abstract Base Class
----------------------------

.. autoclass:: oara.observers.Observer
        :members:

