OARA Standard Goal Messages
===========================

.. autoclass:: oara_std_interfaces.msg.BoolGoal
    :members:


.. autoclass:: oara_std_interfaces.msg.ByteGoal
    :members:


.. autoclass:: oara_std_interfaces.msg.CharGoal
    :members:


.. autoclass:: oara_std_interfaces.msg.EmptyGoal
    :members:


.. autoclass:: oara_std_interfaces.msg.Float32Goal
    :members:


.. autoclass:: oara_std_interfaces.msg.Float64Goal
    :members:


.. autoclass:: oara_std_interfaces.msg.Int8Goal
    :members:


.. autoclass:: oara_std_interfaces.msg.Int16Goal
    :members:


.. autoclass:: oara_std_interfaces.msg.Int32Goal
    :members:


.. autoclass:: oara_std_interfaces.msg.Int64Goal
    :members:


.. autoclass:: oara_std_interfaces.msg.StringGoal
    :members:


.. autoclass:: oara_std_interfaces.msg.UInt8Goal
    :members:


.. autoclass:: oara_std_interfaces.msg.UInt16Goal
    :members:


.. autoclass:: oara_std_interfaces.msg.UInt32Goal
    :members:


.. autoclass:: oara_std_interfaces.msg.UInt64Goal
    :members:

