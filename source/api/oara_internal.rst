OARA Internal Structures
========================

UID
---

Unique IDentifiers for all elements in the OARA architecture (goals, plans, subgoals).


.. autoclass:: oara.internal.uuid.UID


Callback Return Values
----------------------

.. autoenum:: oara.internal.CallbackReturn
        :members:


Event Reaction Strategies
-------------------------

.. autoenum:: oara.internal.event.EventReaction
        :members:


Task
----

.. autoclass:: oara.internal.task.Task
        :members: