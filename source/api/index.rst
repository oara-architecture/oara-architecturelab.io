OARA Python Documentation
=========================


.. toctree::

        oara_modules
        oara_actor
        oara_observer

        oara_internal
        oara_interface
        oara_std_interface


Main function
-------------

.. autofunction:: oara.main

