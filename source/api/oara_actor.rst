OARA Actor
==========

Usefull Methods
---------------

.. rubric:: Creation

.. autosummary::

        oara.actors.Actor


.. rubric:: Goal Lifecycle Management

.. autosummary::

        oara.actors.Actor.select
        oara.actors.Actor.expand
        oara.actors.Actor.commit
        oara.actors.Actor.dispatch
        oara.actors.Actor.monitor
        oara.actors.Actor.evaluate
        oara.actors.Actor.finish
        oara.actors.Actor.drop


.. rubric:: Plan and Task Management

.. autosummary::

        oara.actors.Actor.add_plan
        oara.actors.Actor.get_plan
        oara.actors.Actor.get_goal
        oara.actors.Actor.get_goal_state
        oara.actors.Actor.get_dispatched_goals


.. rubric:: Data and Event Management

.. autosummary::

        oara.actors.Actor.add_data
        oara.actors.Actor.get_data
        oara.actors.Actor.call_data


Actor Abstract Base Class
-------------------------

.. autoclass:: oara.actors.Actor
        :members:
